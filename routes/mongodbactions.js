
var request = require('request');
 var msg = require("./helperbot.js"); 
var mongoose = require('mongoose');
//var customerDetails = require("../models/customerdetails.js"); 
var propDetails= require("../models/OAOPropertyDetail.js");
var testschema=require("../models/OAOApplicantSchema.js");
var sendmailhelper=require("./OAOApplicationHelper.js");
 //var winston = require('winston');


var Schema = mongoose.Schema;
var balanceschema = new Schema({
  accno:  Number,
  balance: Number
});
var balancemodel = mongoose.model('balancemodel', balanceschema);

var self=module.exports={
getBalance:function(req,callback){
  var acno=req.body.result.parameters['number'];
        balancemodel.findOne({ accno: acno }, function(err, result) {
        if(result){
           return callback(result.balance);
        } 
      });
    },
    
sendRemainder:function(cutoff_v,contacted_v,remainders_v){
    console.log("test")
    var remainders="bot_fields.noOfRemaindersSent"
    var contacted="bot_fields.botContacted" ;
         testschema.find( {"bot_fields.noOfRemaindersSent":remainders_v,application_status: 'SAV',mod_time:{$lt: cutoff_v},"bot_fields.botContacted":contacted_v,help_flg:'N'} ,function(err,result) { 
             if(err){
               //logger.error(err);
               console.log(err)
              }
             else if(result){
               console.log(result);
               if(result.length>0){
                for(var i=0;i<result.length;i++){
                  var sender_id = result[i].bot_fields[0].socialId;
                  var crn=result[i].application_id;
                  msg.sendMessage(sender_id,crn,function(success){
                    if(success=="true"){
                      //logger.info("Message send success for sender_id:"+sender_id+" with appn id:"+crn);
                          self.updateContacted(sender_id,crn,(remainders_v+1));
                        }
                  });
                }
              }
             }
        });

},
sendRemainderMail:function(cutoff_v,contacted_v,remainders_v){
    console.log("test")
    var remainders="bot_fields.noOfRemaindersSent"
    var contacted="bot_fields.botContacted" ;
    console.log(cutoff_v+" "+contacted_v+" "+remainders_v)
         testschema.find( {"bot_fields.noOfRemaindersSent":remainders_v,application_status: 'SAV',mod_time:{$lt: cutoff_v},"bot_fields.botContacted":contacted_v,help_flg:'N'} ,function(err,result) { 
             if(err){
               console.log(err)
              }
             else if(result){
               if(result.length>0){
                for(var i=0;i<result.length;i++){
                  var sender_id = result[i].social_id;
                  var crn=result[i].app_num;
                  var email=result[i].email;
                 sendmailhelper.PassMail(email,crn,function(success){ 
                   console.log(success);
                  if(success=="true"){
                    self.updateContacted(sender_id,crn,(remainders_v+1));
                  }else{
                    //  //logger.error(err);
                    console.log(err);
                  }
                });
                  

                }
              }
             }
        });

},

  searchIncmptFields_new:function(){
      propDetails.find({$or: [ { p_type: 'remainder_time' }, { p_type: 'Turn_on_off',property: 'send_msg_config' } ]},function(err,result) { 
        if(err){
          console.log(err);
        }else if(result){
          // console.log(result)
                // chat config
                console.log(result[3].p_value);
                  if((result[3].p_value)=="B" || (result[3].p_value)=="C"){ 
                    var cutoff = new Date();
                    cutoff.setDate(cutoff.getDate()-(result[0].p_value));
                    console.log(cutoff+":: "+result[0].p_value)
                    self.sendRemainder(cutoff,'N','0');//first remainder not contacted
                    self.sendRemainder(cutoff,'Y','0');//first remainder contacted
                    
                    var cutoff_2 = new Date();
                    console.log(cutoff_2+":: "+result[1].p_value)
                    cutoff_2.setDate(cutoff_2.getDate()-(result[1].p_value));
                    self.sendRemainder(cutoff_2,'Y','1');//Second remainder contacted
                   
                    var cutoff_3 = new Date();
                    console.log(cutoff_3+":: "+result[2].p_value)
                    cutoff_3.setDate(cutoff_3.getDate()-(result[2].p_value));
                    self.sendRemainder(cutoff_3,'Y','2');//Third remainder contacted
                  }
                  if((result[3].p_value)=="B" || (result[3].p_value)=="M"){ 
                     var cutoff = new Date();
                    cutoff.setDate(cutoff.getDate()-(result[0].p_value));
                    console.log(cutoff+":: "+result[0].p_value)
                    self.sendRemainderMail(cutoff,'N','0');//first remainder not contacted
                    self.sendRemainderMail(cutoff,'Y','0');//first remainder contacted
                    
                    var cutoff_2 = new Date();
                    console.log(cutoff_2+":: "+result[1].p_value)
                    cutoff_2.setDate(cutoff_2.getDate()-(result[1].p_value));
                    self.sendRemainderMail(cutoff_2,'Y','1');//Second remainder contacted
                   
                    var cutoff_3 = new Date();
                    console.log(cutoff_3+":: "+result[2].p_value)
                    cutoff_3.setDate(cutoff_3.getDate()-(result[2].p_value));
                    self.sendRemainderMail(cutoff_3,'Y','2');//Third remainder contacted
                   
                  }

          
        }
      });
  },

sendIncompleteFieldsMsg:function(req,callback){
  var senderid=req.body.originalRequest.data.sender['id'];
  var crn=req.body.result.parameters['crn'];
  var continue_v="false";
 testschema.find({application_id:crn},function(err,result){
   if(err){console.log(err)}
   else{
     console.log(result);
     self.checkSection(result,function(message,field_t,success,mandatory_sec){
       if(success==true){
        continue_v="true";
        return callback(message,crn,continue_v,field_t,mandatory_sec)
      }else{
        continue_v="false";
        return callback('no',crn,continue_v,field_t,mandatory_sec)
      }
     });
   }
 });
},
checkSection:function(result,callback){
  console.log("checkschema start");
  var j=0;
  var arr_emp="";
  var sec="";
            for(var i=1;i<=result[0].no_of_section;i++){
                     sec="section_"+i;
                      if(result[0].section_EDA[0][sec]==false){
                        console.log(sec);
                         arr_emp=sec;
                         break;
                      }

            }
            console.log(arr_emp);
            if(arr_emp!=""){
                 self.getMandatoryField(result,arr_emp,function(field_t,mandatory_sec){
                console.log(field_t);
                self.getMessage(field_t,function(message,msg_success){
                    console.log(message)
                    if(msg_success=true){
                    var success=true;
                    return callback(message,field_t,success,mandatory_sec);
                  }else{
                    var success=false;
                    return callback(message,field_t,success,mandatory_sec);
                  }
                });
              });
            }else{
                var success=false;
             return callback("no","field_t",success,"mandatory_sec");
            }

          //   for(var i=1;i<=result[0].no_of_section;i++)
          //   {
          //     var sec="section_"+i;
          //     if(result[0].section_EDA[0][sec]==false){
          //     self.getMandatoryField(result,sec,function(field_t,mandatory_sec){
          //       console.log(field_t);
          //       self.getMessage(field_t,function(message,msg_success){
          //           console.log(message)
          //           if(msg_success=true){
          //           var success=true;
          //           return callback(message,field_t,success,mandatory_sec);
          //         }else{
          //           var success=false;
          //           return callback(message,field_t,success,mandatory_sec);
          //         }
          //       });
          //     });
          //    break;
          //   }
          //    var success=false;
          //    return callback("no","field_t",success,"mandatory_sec");
          //  }
          console.log("checkschema end");  
            
},
getMandatoryField:function(result,section,callback){
  var mandatory_sec=section+"_fields";
  var mandatory_sec_field=result[0].Manditory_fields_EDA[0][mandatory_sec][0];
  var sec_var_str=JSON.stringify(mandatory_sec_field);
  var sec_var_json=JSON.parse(sec_var_str)
for (var prop in sec_var_json){
  if(sec_var_json[prop]==false){
    return callback(prop,mandatory_sec);
  }
}
},
getMessage:function(field_t,callback){
  var success=false;
  propDetails.findOne({p_type:'messages',property:field_t},function(err,result_mes) {
          if(err){
            success=false;
             return callback("error occured",success);
          }else{
                success=true;
                return callback(result_mes.p_value,success);
          }         
       });
},
//  sendIncompleteFieldsMsg_new:function(req,callback){
//    self.testnewschema(req)
   
//          var senderid=req.body.originalRequest.data.sender['id'];
//             var crn=req.body.result.parameters['crn'];
//             var arr=[];
//             var arr_emp=[];
//              var continue_v;
//              var prod_type;
//              var field_v;
//             customerDetails.findOne({social_id: senderid,app_num:crn},function(err,result) { 
//             if(result){
//                     prod_type_v=result.prod_type;
//                     propDetails.findOne({p_type:'mandatory_fields',property:prod_type_v},function(err,result_m) { //to find mandatory fields
//                     if(result_m){
//                       arr=result_m.p_value.split(',');
//                        var j=0;
//               for(var i=0;i<arr.length;i++){
               
//                      field_v=arr[i];
//                      	  if(result[field_v]==""){
//                        arr_emp[j]=field_v;
//                        j=j+1;
//                      }
//                  }
//                  console.log(arr_emp)
//                  if(arr_emp.length>0){
//                  propDetails.findOne({p_type:'messages',property:arr_emp[0]},function(err,result_mes) {
//                          continue_v='true';
//                          console.log("Mandatory field added for user with appn number:"+result_mes.p_value);
//                          //logger.info("Mandatory field added for user with appn number:"+result_mes.p_value);
//                         return callback(result_mes.p_value,crn,continue_v,prod_type_v,arr_emp[0]);
//                  });
//                 }else{
//                    continue_v='false';
//                    console.log(continue_v);
//                    return callback('no','0',continue_v,prod_type,arr_emp[0]);
//                 }
//                    }
//                   });//
              
//             }
//             else{
//             console.log(err);
//             }

            
//    });

// },
// insertMissingField:function(req,callback){
//     var missingField=req.body.result.parameters['missingFieldData'];
//     var senderid=req.body.originalRequest.data.sender['id'];
//     var crn=req.body.result.parameters['crn'];
//     var prod_type=req.body.result.parameters['prod_type'];
//     var field_var=req.body.result.parameters['field_var'];
//     var success;
//     console.log(missingField+" "+crn+" "+prod_type+" "+field_var+" "+senderid);
//     console.log([field_var])
//     customerDetails.findOneAndUpdate({social_id: senderid,app_num:crn},
//           {[field_var]:missingField,mod_time:Date.now(),remainders:0}, function (err, docs) {
//             if(docs){
//               //logger.info("Mandatory field added for user with appn number:"+crn);
//                console.log("updated ");
//               return callback(success="true");
//             }else if(err){
//                console.log("err "+err);
//               return callback(success="false");
//             }
         
//           })
 
// },
insertMissingField_test:function(req,callback){
    var missingField=req.body.result.parameters['missingFieldData'];
    var senderid=req.body.originalRequest.data.sender['id'];
    var app_id=req.body.result.parameters['crn'];
    var field_var=req.body.result.parameters['field_var'];
    var success;
    console.log(app_id+" "+[field_var]+" "+missingField)
               request.post({url:'https://sample-latitude-dev-product.herokuapp.com/mongoAPIRoutes/saveOrUpdateNextLoanDetailsForms321',
                    form: {app_id:app_id,
                           [field_var]:missingField,
                            remainder:"0"}}, function(err,body){
                                                    if(err){
                                                        console.log(err);
                                              return callback(success="false");
                                                    }else if(body){
                                                          var success="true";
                                                          console.log("body"+body);
                                                          return callback(success);
                                                    }
                           });
 
},
 updateStatus:function(req,callback){
    var senderid=req.body.originalRequest.data.sender['id'];
    var crn=req.body.result.parameters['crn'];
   request.post({url:'https://sample-latitude-dev-product.herokuapp.com/mongoAPIRoutes/saveOrUpdateNextLoanDetailsForms321',
                    form: {app_id:crn}}, function(err,body){
                                                    if(err){
                                                        console.log(err);
                                              return callback(success="false");
                                                    }else if(body){
                                                          var success="true";
                                                          console.log("body"+body);
                                                          return callback(success);
                                                    }
                           });
//call web api
},
updateHelp:function(req,callback){
    var senderid=req.body.originalRequest.data.sender['id'];
    var app_id=req.body.result.parameters['crn']
    var remainders="bot_fields.noOfRemaindersSent"
    var contacted="bot_fields.botContacted" ;
           testschema.findOneAndUpdate({"bot_fields.socialId":senderid },
          {help_flg:'Y',application_status:"SAV","bot_fields.$.botContacted":'Y',mod_time:Date.now(),"bot_fields.$.noOfRemaindersSent":'0'}, function (err, docs) {
            if(docs){
              return callback(success="true");
            }else if(err){
               console.log("err "+err);
              return callback(success="false");
            }
         
          })
},
updateReason:function(req,callback){
    var senderid=req.body.originalRequest.data.sender['id'];
    var app_id=req.body.result.parameters['crn']
     var reason=req.body.result.parameters['reason'];
     var notinterested_reason="bot_fields.notInterestedReason";
     var contacted="bot_fields.botContacted" ;
           testschema.findOneAndUpdate({"bot_fields.socialId":senderid},
          {"bot_fields.$.notInterestedReason":reason,"bot_fields.$.botContacted":'Y',mod_time:Date.now(),application_status:"CAN"}, function (err, docs) {
            if(docs){
              return callback(success="true");
            }else if(err){
               console.log("err "+err);
              return callback(success="false");
            }
         
          })
},
updateContacted:function(sender_id,crn,remainders_v){
  var remainders="bot_fields.noOfRemaindersSent"
    var contacted="bot_fields.botContacted" ;
           testschema.findOneAndUpdate({application_id:crn},
          {"bot_fields.$.botContacted":'Y',mod_time:Date.now(),"bot_fields.$.noOfRemaindersSent":remainders_v}, function (err, docs) {
            if(docs){
             console.log("Updated contact")
            }else if(err){
               console.log("err "+err);
            }
         
          })
},
}

