
var nodemailer = require('nodemailer');
var random = require("random-js")();
var config_urls = require("../configfile");
var fs = require('fs');
var jade = require('jade');



module.exports = {

    SendMail: function (email, app_id, data, emailTemplateId, callback) {
        var template;
        var mailOptions;
        if (emailTemplateId == 'SAVE_SUBMISSION') {
            console.log('Save Submission');
            template = process.cwd() + '/public/mailtemplate/saveconfirmation.jade';
        } else {
            console.log('Confirmation Submission');
            template = process.cwd() + '/public/mailtemplate/confirmation.jade';
        }
        fs.readFile(template, 'utf-8', function (err, file) {
            if (err) {
                console.log('Error while rendering jade template', template);
            } else {
                var compiledTmpl = jade.compile(file, { filename: template });
                var context = { applicationId: app_id, data: data };
                htmlToSend = compiledTmpl(context);

                /**
                 * node mailer transporter
                 */
                var transporter = nodemailer.createTransport({
                    host: config_urls.url.host,
                    port: config_urls.url.port,
                    secure: true, // use SSL 
                    auth: {
                        user: config_urls.url.gmailID,
                        pass: config_urls.url.gmailPassword
                    }
                });

                 if (emailTemplateId == 'SAVE_SUBMISSION') {
                    mailOptions = {
                        from: config_urls.url.gmailID, 
                        to: email, 
                        subject :'Application Saved Succesfully',
                        html: htmlToSend
                    };
                 }else if(emailTemplateId == 'FINAL_SUBMISSION' && data.product_type == 'Everyday Account'){
                       mailOptions = {
                        from: config_urls.url.gmailID, 
                        to: email, 
                        subject :'Application Processed Succesfully',
                        html: htmlToSend
                    };
                 }else{         
                       mailOptions = {
                        from: config_urls.url.gmailID, 
                        to: email, 
                        subject :'Application Submitted for processing succesfully',
                        html: htmlToSend
                    }; 
                 }
               

                /**
                 *  send mail with defined transport object 
                 */
                transporter.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        console.log('Error while sending mail', error);
                        return callback(Result = "false");
                    }

                    console.log('Message sent: ' + info.response);
                    return callback(Result = "true");
                });
            }
        });
    },

    //Application Reference ID Generation

    RefIdFormater: function (ID) {
        dbSequence = Number(ID);
        var day = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
        return AppRefID = ['DB', year,
            (month > 9 ? '' : '0') + month,
            (day > 9 ? '' : '0') + day,
            dbSequence
        ].join('');

    },

    Gen_coreAcc_no: function (callback) {
        var CORE_ACCOUNT_NUMBNER = "00000" + random.integer(1, 999);
        return callback(CORE_ACCOUNT_NUMBNER);
    },
    Gen_custId: function (callback) {
        var CORE_CUSTOMER_ID = random.integer(100000, 999999);
        return callback(CORE_CUSTOMER_ID);
    },

    BSB_Number: function (callback) {
        return callback(123123);
    }


}; //end of function