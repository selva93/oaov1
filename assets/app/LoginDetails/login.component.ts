import { Component ,AfterViewInit,OnInit} from '@angular/core';
import {Router} from '@angular/router'
import { UserDetailsObject } from "./UserDetailsInterface";
import {OAOService} from "../OAO.Service"
declare var jQuery:any;
declare var Ladda;
@Component({
    selector: 'login',
    templateUrl: './login.component.html'
    
})
export class LoginComponent
{
  model = new UserDetailsObject('', '');
  error_message;
  constructor(private oaoService:OAOService,private router:Router)
  {
       this. onkeyUp();
  }
  onSubmit()
  {
         this.oaoService.Login(this.model).subscribe(
            data => {
                 this.model=data;
                // console.log(data.token);
               if(data.userName==null)
               {
                   
                    this.error_message="Enter the valid user name and password"
                    console.log("failed");
                    this.oaoService.setHasData(false);
                    jQuery('#userName,#password').css('border-color', 'red');
                    jQuery('#loginSubmit').prop('disabled', true);
               }
               else
               {
                   //console.log(data.result)
                    console.log("success")
                    this.oaoService.setHasData(true);
                    this.oaoService.setUserDeatils(this.model);

                   
                    console.log(this.oaoService.getFindLogin())
                    if(this.oaoService.getFindLogin())
                    { 
                        console.log("home")
                        this.router.navigate(['/home']);
                    }else{
                          console.log("Dashboard")
                          this.router.navigate(['/Dashboard']);
                    }

                  
               }
            },
            error => console.log("ERROR:"+error),
            () => console.log());
    }

    ngOnInit(){
        this. onkeyUp();
    }

    onkeyUp()
    {  
      if(this.model.userName==null || this.model.userName=="" || this.model.password==null ||this.model.password=="" )
      {    
            jQuery('#loginSubmit').prop('disabled', true);
      }
      else{
            
             jQuery('#loginSubmit').prop('disabled', false);
      }
    }

}
