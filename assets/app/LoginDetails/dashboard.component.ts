import { Component ,AfterViewInit,OnInit} from '@angular/core';
import {Router} from '@angular/router'
import { UserDetailsObject } from "./UserDetailsInterface";
import {OAOService} from "../OAO.Service"
@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.Component.html'
    
})
export class DashboardComponent
{
     userDetailsObject; //chandan
     fName;
     userId;
     btn;
     constructor(private oaoService:OAOService,private router:Router){  }
     
  ngOnInit() 
 {    
    if(this.oaoService.getHasData()){
        console.log(this.oaoService.getHasData())
        this.btn=true;
        this.oaoService.GetLoginUserDetails(this.oaoService.getUserDeatils()).subscribe(
            data => {
                    this.oaoService.setUserDeatils(data);
                    this.userDetailsObject=this.oaoService.getUserDeatils();
                    console.log(this.userDetailsObject);
                
                    this.userId=this.userDetailsObject.result.userId;
                    var str=this.userDetailsObject.result.fName
                    str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                    return letter.toUpperCase();
                        });
                    this.fName=str;
                });      
    }      
 }
 logout()
 {
    this.oaoService.logout();
 }
}