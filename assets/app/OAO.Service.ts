import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import 'rxjs/Rx';
import { Observable } from "rxjs";
import { PersonalDetails } from "./PersonalDetailsInterface";

import { UserDetailsObject } from "./LoginDetails/UserDetailsInterface"; //chandan

@Injectable()
export class OAOService
{

   // private baseURL:String = "http://localhost:3000";
 private baseURL:String = "https://oaoapp.herokuapp.com";
    data:PersonalDetails;
    
    userData = new UserDetailsObject('', '');  //chandan
    hasData:boolean; //chandan
    findLogin:boolean=false;//chandan
    constructor(private http: Http) {}
	//-------------onlineIdcheck--------------------------------------------
      onlineIdcheck(user: PersonalDetails) {
        const body = JSON.stringify(user);
        console.log("onlineIdcheck"+body);
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(`${this.baseURL}/idcheck/onlineidcheck`, body, {headers: headers})
            .map((response: Response) => response.json())
    }
//------------------By Rajath-------------
//chandan
    Login(user:UserDetailsObject)
    {
        const body = JSON.stringify(user);
        console.log("service Login()"+body);
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(`${this.baseURL}/loginAPI/login`, body, {headers: headers})
            .map((response: Response) => response.json())
    }

    GetLoginUserDetails(user:UserDetailsObject)
    {
        const body = JSON.stringify(user);
        console.log("service GetUserDetails()"+body);
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(`${this.baseURL}/loginAPI/getLoginUserDetails`, body, {headers: headers})
            .map((response: Response) => response.json())
    }

    setUserDeatils(userData:UserDetailsObject){
        console.log("setUserDeatils()"+userData)
        this.userData=userData;
    }
    getUserDeatils(){
          console.log("getUserDeatils"+this.userData);
        return this.userData;
    }

    setHasData(hasData:boolean){
        this.hasData=hasData;
    }
    getHasData(){
        return this.hasData;
    }

    setFindLogin(findLogin:boolean){
        this.findLogin=findLogin;
    }
    getFindLogin(){
        return this.findLogin;
    }

    registerInternetBanking(user:UserDetailsObject)
    {
        const body = JSON.stringify(user);
        console.log("service registerInternetBanking()"+body);
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(`${this.baseURL}/loginAPI/regIntBanking`, body, {headers: headers})
            .map((response: Response) => response.json());
    }

    logout(){
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.get(`${this.baseURL}/loginAPI/logout`, {headers: headers})
            .map((response: Response) => response.json());
    }

    checkDup(user:PersonalDetails){
        const body = JSON.stringify(user);
        console.log("service registerInternetBanking()"+body);
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(`${this.baseURL}/loginAPI/checkDup`, body, {headers: headers})
            .map((response: Response) => response.json());
    }
//chandan

     OAOCreateOrUpdateApplicant(user: PersonalDetails) {
        const body = JSON.stringify(user);
        console.log("service"+body);
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(`${this.baseURL}/api/Applicants`, body, {headers: headers})
            .map((response: Response) => response.json())
    }
    OAOCreateOrUpdateHomeloanApplicant(user: PersonalDetails) {
        const body = JSON.stringify(user);
        console.log("service"+body);
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(`${this.baseURL}/api/HomeLoanApplicants`, body, {headers: headers})
            .map((response: Response) => response.json())
    }
    OAOCreateOrUpdatePersonalloanApplicant(user: PersonalDetails) {
        const body = JSON.stringify(user);
        console.log("service"+body);
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(`${this.baseURL}/api/PersonalLoanApplicants`, body, {headers: headers})
            .map((response: Response) => response.json())
    }

    GetPropertyDetails(PropertyType:String,Property:String){
         return this.http.get(`${this.baseURL}/api/PropertyDetails/`+PropertyType+'/'+Property)
            .map((response: Response) => response.json())
    }

      GetApplicantsDetail(Applicants_id:String){
        // console.log("apppp"+Applicants_id);
         return this.http.get(`${this.baseURL}/api/ApplicantsRecord/`+Applicants_id)
            .map((response: Response) => response.json())
            .catch((error: Response) => Observable.throw(error.json()));
    }

    setData(data:PersonalDetails){
        this.data=data;
    }
    getData(){
        return this.data;
    }
    getConfig(){
     return this.http.get(`${this.baseURL}/api/getConfig`)
            .map((response: Response) => response.json())
    
    }
    prod_type:string
    updatesection(section:String,app_id:String){
        this.prod_type=localStorage.getItem('prod_code');
        return this.http.get(`${this.baseURL}/api/UpdateSection/`+app_id+'/'+section+'/'+this.prod_type)
            .map((response: Response) => response.json())
    }

    setFb:boolean=false;
    setFbData(set:boolean){
        this.setFb=set;
    }
    getFbData(){
        return this.setFb;
    }



} 
