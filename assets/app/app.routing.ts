import { Routes, RouterModule } from "@angular/router";

import {DashboardComponent} from './LoginDetails/dashboard.component' // chandan

import { AppComponent } from "./app.component";
import {LoginComponent} from './LoginDetails/login.component' //chandan

import {PersonaldetailsBasicComponent} from "./Everyday/personaldetailsbasic.component";
import {PersonaldetailsContactComponent} from "./Everyday/personaldetailscontact.component";
import {OnlineIdCheck} from "./Everyday/onlineIDcheck.component"
import {TaxInformation} from "./Everyday/taxinfo.component"
import {PasswordSetup} from "./Everyday/passwordSetup.component"
import {PropertyDetails} from './HomeLoanRequirements/PropertyDetails.component'//Home Loan
import {LoanDetails} from './HomeLoanRequirements/loandetails.component'
import {LoanSummary} from './HomeLoanRequirements/loansummary.component'
import {IncomeExpense} from './HomeLoanRequirements/incomeexpense.component'
import {AssetsComponent} from './HomeLoanRequirements/assets.component'
import {LoanDetailsPersonal} from './PersonalLoanRequirements/loandetailspersonal.component'//Personal Loan

const APP_ROUTES: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: AppComponent },//
    { path: 'Login', component: LoginComponent },//
    { path: 'PersonalBasicInfo', component: PersonaldetailsBasicComponent },//
    { path: 'PersonalContactInfo', component: PersonaldetailsContactComponent },//
    { path: 'TaxInformation', component: TaxInformation },//
    { path: 'OnlineIdCheck', component: OnlineIdCheck },//
    { path: 'PasswordSetup', component: PasswordSetup },//
    { path: 'PropertyDetails', component: PropertyDetails },
    { path: 'LoanDetails', component: LoanDetails },
    { path: 'LoanSummary', component: LoanSummary },
    { path: 'IncomeExpense', component: IncomeExpense },
    { path: 'Assets', component: AssetsComponent },
    { path: 'LoanDetailsPersonal', component: LoanDetailsPersonal },
    { path: 'Dashboard', component: DashboardComponent }

   
    
];

export const routing = RouterModule.forRoot(APP_ROUTES);