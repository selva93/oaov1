import { Component, AfterViewInit, OnInit } from '@angular/core';
import { LoanReason } from "./../loanreason.interface";
import { PersonalDetails } from "./../personaldetailsinterface";
import { IncomeExpense } from "./../HomeLoanRequirements/incomeexpense.component";
import { Router } from '@angular/router';
import { ConfigDetails } from "../configinterface";
import { OAOService } from "../OAO.Service"
declare var jQuery:any;
// declare var Ladda:any;
@Component({
    selector: 'loandetailspersonal',
    templateUrl: './loandetailspersonal.component.html'

})
export class LoanDetailsPersonal implements OnInit {
    model = new PersonalDetails('', '', '', '', '', '', '');
    public application_id: any;
    prod_type: string
    prod_code: string
    configMsg: ConfigDetails;
    public loanTerm: any[] = [];
    public repaymentType: any[] = [];
    public loanreason = [];
    public loan_reason_v=new LoanReason(false,false,false,false,false,false,false);
    isLoading:Boolean=false;
    constructor(private oaoService: OAOService, private router: Router) {
        this.model.frequencyType = "Monthly";
        this.prod_type = localStorage.getItem('prod_type');
        this.prod_code = localStorage.getItem('prod_code');
        this.application_id = localStorage.getItem('application_id');
        this.oaoService.getConfig()
            .subscribe((data) => { this.configMsg = JSON.parse(JSON.stringify(data.data)); });

    }
    ngOnInit() {
        jQuery('input:visible:first').focus();
        this.oaoService.GetApplicantsDetail(localStorage.getItem('application_id'))
            .subscribe(
            data => {
                this.model = data.result[0];
                if (this.model.frequencyType == null) {
                    this.model.frequencyType = "Monthly";
                }
                if (this.model.loanterm == null) {
                    this.model.loanterm = '0'
                }
                if (this.model.repaymenttype == null) {
                    this.model.repaymenttype = '0'
                }
                if(this.model.loanreason.length>0){
                this.loan_reason_v = this.model.loanreason[0];
				}

            }
            );
        this.oaoService.GetPropertyDetails('commonCodes', 'LOAN_TERM')
            .subscribe(
            data => {
                var count = Object.keys(data.result).length;
                for (var i = 0; i < count; i++) {
                    this.loanTerm.push({
                        prop_desc: data.result[i].property_desc,
                        prop_val: data.result[i].property_value
                    })
                }
            }
            );
        this.oaoService.GetPropertyDetails('commonCodes', 'REPAYMENT_TYPE')
            .subscribe(
            data => {
                var count = Object.keys(data.result).length;
                for (var i = 0; i < count; i++) {
                    this.repaymentType.push({
                        prop_desc: data.result[i].property_desc,
                        prop_val: data.result[i].property_value
                    })
                }
            }
            );
    }

    onSubmit(formRecord: PersonalDetails) {
        this.isLoading=!this.isLoading;
        formRecord.app_id = this.application_id;
        formRecord.loanreason = this.loan_reason_v;
        if((!String(formRecord.amtborrow).match(/\./g) || String(formRecord.amtborrow).match(/\./g)) && String(formRecord.amtborrow).match(/\,/g) ){
        var amtborrow = formRecord.amtborrow.replace(/\,/g,"");
        formRecord.amtborrow=amtborrow;
        }

        console.log(formRecord);
        this.oaoService.OAOCreateOrUpdatePersonalloanApplicant(formRecord)
            .subscribe(
            data => {
                console.log("sample" + data);
                this.router.navigate(['/IncomeExpense']);
            }
            );

    }
    // setReason(reason: string, check: any) {
    //     var loan = {
    //         'reason': reason
    //     };
    //     if (check == true) {
    //         this.loanreason.push(loan);
    //     } else {
    //         for (var i = 0; i < this.loanreason.length; i++) {
    //             if (this.loanreason[i]['reason'] == reason) {
    //                 this.loanreason.splice(i, 1);
    //             }
    //         }

    //     }


    // }
    updateSection() {
        this.oaoService.updatesection("section_1", localStorage.getItem('application_id')).subscribe(
            data => {
                console.log(data);
                console.log("updated");
                this.router.navigate(['/PersonalContactInfo']);
            }
        );
    }
  AmountFormatter(amountvalue: any, var_v: any) {
       if( typeof amountvalue != 'undefined' && amountvalue!=null && amountvalue!=''  ){
            console.log("asd "+amountvalue+" "+var_v)
            var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'AUD',
            minimumFractionDigits: 2,
        });
        //     this.testmodel[var_v]="";
        //  this.testmodel[var_v]=amountvalue;
        var finalString = formatter.format(amountvalue);
		finalString = finalString.replace('A$','');
        this.model[var_v] = finalString.replace('$','');
    }else{
         this.model[var_v]="0.0";
    }
    }

   revert(oldvalue:any,var_v: any){
        var tmpOldvalue;
       if(oldvalue!=null && String(oldvalue).match(/\,/g)){
        tmpOldvalue=oldvalue.replace(/\,/g,'');
        console.log(tmpOldvalue);
        this.model[var_v]=tmpOldvalue.substr(0,tmpOldvalue.length-3);
        console.log(this.model[var_v]);
        }
    }


}