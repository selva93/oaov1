import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { routing } from "./app.routing";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from '@angular/http';
import { LaddaModule } from 'angular2-ladda';

import {AppComponent} from './app.component'
import {AppLogin } from './home.component'
import {LoginComponent} from './LoginDetails/login.component' // chandan
import {DashboardComponent} from './LoginDetails/dashboard.component' // chandan
import {HeaderComponent} from './header.component'
import {FooterComponent} from './footer.component'

import {MedicareValidator} from './medicare_validator'
import { SplitValidator } from './fixedandvariable';
import { FirstNameValidator } from './namevalidator';
import { MobileNumberValidator } from './mob_number';
import { EmailValidator } from './email_validator';
import { TFNValidator } from './tfnvalidator';
import { PassportValidator } from './passport_validator';
import { DrivingLicenceValidator } from './Driving_licence_validator';
import { dobvalidator } from './dob_validator';
import {PropertyDetails} from './HomeLoanRequirements/PropertyDetails.component'
import {LoanDetails} from './HomeLoanRequirements/loandetails.component'
import {LoanSummary} from './HomeLoanRequirements/loansummary.component'
import {IncomeExpense} from './HomeLoanRequirements/incomeexpense.component'
import {AssetsComponent} from './HomeLoanRequirements/assets.component'
import { Common } from './commonFunc';
import { Md2Module }  from 'md2';
import { DatePipe } from '@angular/common';

import {PersonaldetailsBasicComponent} from "./Everyday/personaldetailsbasic.component";
import {SecondContainerComponent} from "./secondContainer.component";
import {PersonaldetailsContactComponent} from "./Everyday/personaldetailscontact.component"
import {OnlineIdCheck} from "./Everyday/onlineIDcheck.component"
import {TaxInformation} from "./Everyday/taxinfo.component"
import {PasswordSetup} from "./Everyday/passwordSetup.component"
import {LoanDetailsPersonal} from './PersonalLoanRequirements/loandetailspersonal.component'//Personal Loan
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'

import {OAOService} from "./OAO.Service"
import {GooglePlaceModule} from 'ng2-google-place-autocomplete';
import { FacebookModule } from 'ng2-facebook-sdk'

@NgModule({
    declarations: [
        DashboardComponent,//chandan
        AppComponent,
        PersonaldetailsBasicComponent,
        PersonaldetailsContactComponent,
        AppLogin,
        TaxInformation,
        SecondContainerComponent,
        OnlineIdCheck,
        HeaderComponent,
        PasswordSetup,
        LoginComponent,
		FirstNameValidator,
        MobileNumberValidator,
        EmailValidator,
        PassportValidator,
        DrivingLicenceValidator,
        dobvalidator,
        FooterComponent,
        PropertyDetails,
        LoanDetails,
        LoanSummary,
        IncomeExpense,
        AssetsComponent,
        LoanDetailsPersonal,
        SplitValidator,
        TFNValidator,
        MedicareValidator,
		],
    imports: [
        BrowserModule,
        routing,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        FacebookModule,
        GooglePlaceModule,
		 Md2Module.forRoot(),
        LaddaModule.forRoot({
            style: "contract",
            spinnerSize: 40,
            spinnerColor: "grey",
            spinnerLines: 12
        })
        ],
        providers:[OAOService,Common,DatePipe],
    
    bootstrap: [AppLogin]
})
export class AppModule {

}