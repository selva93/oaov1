import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PersonalDetails } from "../personaldetailsinterface";
import { ConfigDetails } from "../configinterface";
import { OAOService } from "../OAO.Service"
import { FirstNameValidator } from "../namevalidator"
import { FormGroup, FormControl, Validators, NgForm } from "@angular/forms";
import { Common } from '../commonFunc';
import { DatePipe } from '@angular/common';

import { UserDetailsObject } from "../LoginDetails/UserDetailsInterface"; //chandan
declare var jQuery: any;
declare var Ladda: any;
@Component({
    selector: 'personaldetailsbasic',
    templateUrl: './personaldetailsbasic.component.html',
	providers: [DatePipe]


})
export class PersonaldetailsBasicComponent implements AfterViewInit, OnInit {
    public details: any;
    public items: number[] = [];
    public application_id: any;
    date_v = new Date();
    min_year: Number;
    max_year: Number;
    min_age: number;
    model = new PersonalDetails('', '', '', '', '', '', '');
    model_v = new PersonalDetails('', '', '', '', '', '', '');
    configMsg: ConfigDetails
    test: boolean
    prod_type: string
    prod_code: string
    singleORjoint: string
    myForm: FormGroup;
    isLoading: boolean = false;

    checkData:boolean=false; //chandan
    userDetailsObject; //chandan
     /**Initialization for md2 date component */
        isRequired = false;
        isDisabled = false;
        isOpenOnFocus = false;
        isOpen = false;
        today: Date = new Date();
        type: string = 'date';
        types: Array<any> = [
            { text: 'Date', value: 'date' },
            { text: 'Time', value: 'time' },
            { text: 'Date Time', value: 'datetime' }];

        mode: string = 'auto';
        modes: Array<any> = [
            { text: 'Auto', value: 'auto' },
            { text: 'Portrait', value: 'portrait' },
            { text: 'Landscape', value: 'landscape' }];

        container: string = 'inline';
        containers: Array<any> = [
            { text: 'Inline', value: 'inline' },
            { text: 'Dialog', value: 'dialog' }];

        date: Date = null;
        minDate: Date = null;
        maxDate: Date = null;
      

        openDatepicker() {
            this.isOpen = true;
            setTimeout(() => {
                this.isOpen = false;
            }, 1000);
        }
        /**end of md2 component */
    constructor(private oaoService: OAOService, private router: Router, private route: ActivatedRoute,private datePipe: DatePipe) 
    {
        this.min_age = 0;
        this.test = false;
        this.date_v = new Date()
        this.prod_type = localStorage.getItem('prod_type');
        this.prod_code = localStorage.getItem('prod_code');
        this.singleORjoint = localStorage.getItem('singleORjoint');
        console.log(this.prod_type)
        console.log(this.prod_code)
        // this.route.params.subscribe(params => {
        //                         this.prod_type = params['prod_type'];
        //                         this.prod_code = params['prod_code'];
        //                         localStorage.setItem('prod_type', this.prod_type);
        //                         localStorage.setItem('prod_code', this.prod_code);
        //                       });
        this.oaoService.GetPropertyDetails('GENERIC_PROP', 'MIN_YEAR')
            .subscribe(
            data => {
                this.min_year = data.result[0].property_value;
                console.log(this.min_year);
            }
            );
        this.oaoService.GetPropertyDetails('GENERIC_PROP', 'DOB')
            .subscribe(
            data => {
                this.min_age = data.result[0].property_value;
                console.log(this.max_year);
                var mon = this.date_v.getMonth() + 1;
                var year = this.date_v.getFullYear() - this.min_age;
                this.max_year = year
                console.log(this.max_year)
                //  this.model.dob=this.date_v.getDate()+"/"+mon+"/"+this.max_year;
            }
            );

        this.model.title = '0'
        this.oaoService.GetApplicantsDetail(localStorage.getItem('application_id'))
            .subscribe(
            data => {
                this.model_v = data.result[0];
                console.log("model_V");
                console.log(this.model_v)
                this.application_id = localStorage.getItem('application_id');
                if (this.application_id != null) {
                    this.model = this.model_v;
                }
            }
            )
        this.oaoService.getConfig()
            .subscribe((data) => { this.configMsg = JSON.parse(JSON.stringify(data.data)); });

        // this.application_id =localStorage.getItem('application_id');
        // // this.model_v=this.oaoService.getData();
        // console.log(this.model_v)
        // if(this.application_id!=null){
        // this.model=this.model_v;
        // }

    }

    onSubmit(formRecord: PersonalDetails) {
         if(this.checkData){
            formRecord.existing_cust_status="Y";
            }else{
                formRecord.existing_cust_status="N";
            }
        formRecord.app_id = this.application_id;
        formRecord.product_type = this.prod_type;
        formRecord.singleORjoint = this.singleORjoint
        console.log('Form Record...',formRecord);
        var formatedDate = this.datePipe.transform(formRecord.dob,'MM/dd/yyyy');
        console.log('Formated date',formatedDate);
        formRecord.dob = formatedDate;

        // if(this.application_id!=null){
        //formRecord.app_id=this.application_id;
        // }
        switch (this.prod_code) {

            case 'EVR': this.oaoService.OAOCreateOrUpdateApplicant(formRecord)
                .subscribe(
                data => {
                    // this.oaoService.setData(data.Result);
                    console.log("sample" + data);
                    localStorage.setItem('application_id', data.Result.application_id);
                    this.router.navigate(['/PersonalContactInfo']);
                }
                );
                break;
            case 'HML': this.oaoService.OAOCreateOrUpdateHomeloanApplicant(formRecord)
                .subscribe(
                data => {
                    // this.oaoService.setData(data.Result);
                    console.log("sample" + data);
                    localStorage.setItem('application_id', data.Result.application_id);
                    this.router.navigate(['/PersonalContactInfo']);
                }
                );
                break;
            case 'PRL': this.oaoService.OAOCreateOrUpdatePersonalloanApplicant(formRecord)
                .subscribe(
                data => {
                    // this.oaoService.setData(data.Result);
                    console.log("sample" + data);
                    localStorage.setItem('application_id', data.Result.application_id);
                    this.router.navigate(['/PersonalContactInfo']);
                }
                );
                break;
            default: console.log("Page not found");

        }

         this.isLoading = !this.isLoading;
    }


    ngOnInit() 
    {
        //for fb data
        if(this.oaoService.getFbData()==true){
            this.model=this.oaoService.getData();
            if(this.model.title==null || this.model.title==''){
                    this.model.title='0';
            }
        }

         //chandan 
        console.log("PersonaldetailsBasicComponent ngOnInit()") 
        this.checkData=this.oaoService.getHasData(); 
        if(this.checkData) //pre-populating userDetails
        {
            this.userDetailsObject=this.oaoService.getUserDeatils();
            this.model.title=this.userDetailsObject.result.title;
            var str=this.userDetailsObject.result.fName
            str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                 return letter.toUpperCase();
                    });
            this.model.fname=str;
            this.model.mname=this.userDetailsObject.result.mName;
            this.model.lname=this.userDetailsObject.result.lName;
            this.model.dob=this.userDetailsObject.result.dob;

             //jQuery('#title option:not(:selected)').prop('disabled', true);
                jQuery('#title').prop('disabled', true);
                jQuery('#dob').prop('disabled', true);
             
             jQuery('#fname').attr('readonly', 'true');
             jQuery('#mname').attr('readonly', 'true');
             jQuery('#lname').attr('readonly', 'true');  
        }
        //chandan

        this.test = true;
        this.oaoService.GetPropertyDetails('commonCodes', 'SAL')
            .subscribe(
            data => {
                var count = Object.keys(data.result).length;
                for (var i = 0; i < count; i++) {
                    this.items.push(data.result[i].property_desc)
                }
            }
            );

    }


    clear()
    {
        console.log("clear");
        this.oaoService.setHasData(false);
    }
    dob_valid: number;
    dob_err: string;
    dispDate(dob: any) {
        this.model.dob = dob;
        // this.dob_valid=dob-this.min_age;
        // console.log(this.dob_valid)
        // if(this.dob_valid>=18){
        //     this.model.dob=dob;
        // }
    }
    ngAfterViewInit() {

       // jQuery('input:visible:first').focus();
        jQuery('select:first').focus();
        var options = {
            format: "dd/mm/yyyy",
        }
        if (jQuery('.datepicker') && jQuery('.datepicker').length) {
            jQuery('.datepicker').dateDropper(options);
        }
        jQuery('body').on('change', '#dob', function () {
            jQuery('#dob').trigger('click');
        });
    }
    


}


