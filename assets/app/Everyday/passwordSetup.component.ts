import { Component ,AfterViewInit,OnInit} from '@angular/core';
import {Router} from '@angular/router'
import { PersonalDetails } from "../personaldetailsinterface";
declare var jQuery:any;
import { OAOService } from "../OAO.Service"
import { UserDetailsObject } from "../LoginDetails/UserDetailsInterface"; //chandan
declare var Ladda
@Component({
    selector: 'passwordSet',
    templateUrl: './passwordSet.html'
    
})
export class PasswordSetup{
       
        private check:boolean=false;
        model = new UserDetailsObject('','');
        pwd;
        rpwd;
        status;
    constructor(private oaoService: OAOService,private router:Router){
        //call get method to get previous form value
    }
      onSubmit(){
        // this.check=true
       //call set method to hold form value
       //call api to validate id then show msg
       //if set is success call showSave()
        if(this.pwd==this.rpwd)
        {

             this.model.password =this.pwd;
             this.oaoService.registerInternetBanking(this.model).subscribe(
                data => {
                    this.status=data;  
                });
            jQuery('#success-modal').modal('show'); //chandan

        }
        else{
            
        }
    }

    onKeyUp()
    {
        console.log("onKeyUp");

          if(this.pwd==this.rpwd)
        {
            console.log("pwd==rpwd");
             jQuery('#continue').prop('disabled', false);
             jQuery('#rpwd').css('border-color', 'green');
        }else{
            jQuery('#rpwd').css('border-color', 'red');
            jQuery('#continue').prop('disabled', true);
        }

    }

   FindLogin()
  {
      this.oaoService.setFindLogin(false);

  }

  ngOnInit()
  {
      this.oaoService.setFindLogin(false);
       jQuery('#continue').prop('disabled', true);
        console.log(localStorage.getItem('application_id'));
       this.oaoService.GetApplicantsDetail(localStorage.getItem('application_id')).subscribe(
            data => {
                this.model.userId = data.result[0].core_customer_id;
                this.model.userName = data.result[0].core_customer_id;
                this.model.title = data.result[0].title;
                this.model.fName = data.result[0].fname;
                this.model.mName = data.result[0].mname;
                this.model.lName = data.result[0].lname;
                this.model.dob = data.result[0].dob;
                this.model.age=this.getAge((data.result[0].dob)).toString();
                this.model.email = data.result[0].email;
                this.model.mobile = data.result[0].mobile;
                this.model.TFN = data.result[0].tfn
                this.model.exemptionReason = data.result[0].exemption;
                this.model.homeAddress= data.result[0].address;
                this.model.postalAddress= data.result[0].paddress;
            });  
     localStorage.clear();
  }
    

    getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
}