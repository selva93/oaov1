import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { ConfigDetails } from "../configinterface";
import { PersonalDetails } from "../personaldetailsinterface";
import { OAOService } from "../OAO.Service";
import { DatePipe } from '@angular/common';

declare var jQuery: any;
declare var Ladda
@Component({
    selector: 'onlineidcheck',
    templateUrl: './onlineidcheck.html',
	providers: [DatePipe]

})
export class OnlineIdCheck implements OnInit {
    model = new PersonalDetails('', '', '', '', '', '', '');
    public items: any[] = [];
    public cardColor: any[] = [];
    public state: any[] = [];
    private check: boolean = false;
    idCheck_v: String;
    public inf_002: String;
    public inf_006: String;
    public inf_007: String;
    public passport_check: String;
     public DL_check: String;
      public Medicare_check: String;
    max_year: Number;
    public inf_003: String;
    public wrn_002: String;
    application_id: any;
    configMsg: ConfigDetails
    prod_type: string;
    prod_code: string;
    public inf_loan: string;
    date_v = new Date();
     isLoading: boolean = false;
	 
	  /**Initialization for md2 date component */
        isRequired = false;
        isDisabled = false;
        isOpenOnFocus = false;
        isOpen = false;
        today: Date = new Date();
        type: string = 'date';
        types: Array<any> = [
            { text: 'Date', value: 'date' },
            { text: 'Time', value: 'time' },
            { text: 'Date Time', value: 'datetime' }];

        mode: string = 'auto';
        modes: Array<any> = [
            { text: 'Auto', value: 'auto' },
            { text: 'Portrait', value: 'portrait' },
            { text: 'Landscape', value: 'landscape' }];

        container: string = 'inline';
        containers: Array<any> = [
            { text: 'Inline', value: 'inline' },
            { text: 'Dialog', value: 'dialog' }];

        date: Date = null;
        minDate: Date = null;
        maxDate: Date = null;
      

        openDatepicker() {
            this.isOpen = true;
            setTimeout(() => {
                this.isOpen = false;
            }, 1000);
        }
        /**end of md2 component */
		
    constructor(private oaoService: OAOService, private router: Router,private datePipe: DatePipe) {
        this.prod_type = localStorage.getItem('prod_type');
        this.prod_code = localStorage.getItem('prod_code');
        //   this.model=this.oaoService.getData();
        //   this.application_id=this.model.application_id

        this.max_year = 0;
        this.application_id = localStorage.getItem('application_id');
        this.oaoService.getConfig()
            .subscribe((data) => {
            this.configMsg = JSON.parse(JSON.stringify(data.data));
            }
            );



    }
 onidcheck(formRecord: PersonalDetails) {
         this.isLoading = !this.isLoading;
        formRecord.app_id = this.application_id;
        formRecord.skip = true;
		var formatedDate = this.datePipe.transform(formRecord.validTo,'MM/dd/yyyy');
        console.log('Formated date id Check',formatedDate);
        formRecord.validTo = formatedDate;

         console.log("Rajath")
                console.log(formRecord);

                this.oaoService.onlineIdcheck(formRecord)
                    .subscribe(
                    data => {
                         if(data.pass=="success" || data.dl=="success"|| data.mc=="success")
                         {
                            this.passport_check=data.pass
                            this.DL_check=data.dl
                            this.Medicare_check=data.mc
                            jQuery('#onlineidcheck').modal('show');
                        }
                         else
                         {
                              this.passport_check="passport not verified"
                            this.DL_check="Dl not verified"
                            this.Medicare_check="medicare not verified"
                                jQuery('#error').modal('show');
                         }
                    }
                    );
 }

    onSubmit(formRecord: PersonalDetails) {
        formRecord.app_id = this.application_id;
        formRecord.skip = true;
		var formatedDate = this.datePipe.transform(formRecord.validTo,'MM/dd/yyyy');
        console.log('Formated date on Submit',formatedDate);
        formRecord.validTo = formatedDate;
        //  this.oaoService.OAOCreateOrUpdateApplicant(formRecord)
        //         .subscribe(
        //             data => {
        // 		// this.oaoService.setData(data.Result);
        //             console.log("data"+JSON.stringify(data));
        //             if(this.prod_code=='EVR'){
        //                     this.check=true;
        //                     this.showSave();
        //                 }else{
        //                      this.successLoan();
        //                 }
        // 			});


        switch (this.prod_code) {

            case 'EVR': this.oaoService.OAOCreateOrUpdateApplicant(formRecord)
                .subscribe(
                data => {
                    // this.oaoService.setData(data.Result);
                    this.check = true;
                    this.showSave();
                }
                );
                break;
            case 'HML': this.oaoService.OAOCreateOrUpdateHomeloanApplicant(formRecord)
                .subscribe(
                data => {
                    // this.oaoService.setData(data.Result);
                    this.successLoan();
                }
                );
                break;
            case 'PRL': this.oaoService.OAOCreateOrUpdatePersonalloanApplicant(formRecord)
                .subscribe(
                data => {
                    // this.oaoService.setData(data.Result);
                    this.successLoan();
                }
                );
                break;
            default: console.log("Page not found");

        }
    }


    showSave() {
        if (this.check == true) {
            this.oaoService.GetApplicantsDetail(localStorage.getItem('application_id'))
                .subscribe(
                data => {
                    this.model = data.result[0];
                    localStorage.clear();
                    jQuery('#success-1').modal('show');
                });

        }
    }
    public inf_code: string = '';
    successLoan() {
        localStorage.clear();
        if (this.prod_code == 'HML') {
            this.inf_code = 'INF_004'
        } else {
            this.inf_code = 'INF_005'
        }
        //Info message 004
        this.oaoService.GetPropertyDetails('INFO_MESSAGE', this.inf_code)
            .subscribe(
            data => {
                this.inf_loan = data.result[0].property_value;
                jQuery('#success_loan').modal('show');
            }
            );

    }

    ngOnInit() {

        this.oaoService.GetApplicantsDetail(localStorage.getItem('application_id'))
            .subscribe(
            data => {
                this.model = data.result[0];
                if (this.model.idstate == null) {
                    this.model.idstate = '0';
                }
                if (this.model.color == null) {
                    this.model.color = '0';
                }
                if (this.model.DLidState == null) {
                    this.model.DLidState = '0';
                }
            }
            );

        this.oaoService.GetPropertyDetails('commonCodes', 'COUNTRY')
            .subscribe(
            data => {
                var count = Object.keys(data.result).length;
                for (var i = 0; i < count; i++) {
                    this.items.push({
                        prop_desc: data.result[i].property_desc,
                        prop_val: data.result[i].property_value
                    })
                }
                this.items.sort();
            }
            )

        this.oaoService.GetPropertyDetails('commonCodes', 'CRDCLR')
            .subscribe(
            data => {
                var count = Object.keys(data.result).length;
                for (var i = 0; i < count; i++) {
                    this.cardColor.push({
                        prop_desc: data.result[i].property_desc,
                        prop_val: data.result[i].property_value
                    })
                }
            }
            )

        this.oaoService.GetPropertyDetails('commonCodes', 'STATE')
            .subscribe(
            data => {
                var count = Object.keys(data.result).length;
                for (var i = 0; i < count; i++) {
                    this.state.push({
                        prop_desc: data.result[i].property_desc,
                        prop_val: data.result[i].property_value
                    })
                }
            }
            );

        this.oaoService.GetPropertyDetails('turnOnOff', 'idCheck')
            .subscribe(
            data => {
            this.idCheck_v = data.result[0].property_value
                console.log(this.idCheck_v)
            }
            );
        //Info message 003
        this.oaoService.GetPropertyDetails('INFO_MESSAGE', 'INF_003')
            .subscribe(
            data => {
                this.inf_003 = data.result[0].property_value;
            }
            );
        //Info message 002
        this.oaoService.GetPropertyDetails('INFO_MESSAGE', 'INF_002')
            .subscribe(
            data => {
                this.inf_002 = data.result[0].property_value;
            }
            );
         //Info message 006
        this.oaoService.GetPropertyDetails('INFO_MESSAGE', 'INF_006')
            .subscribe(
            data => {
                this.inf_006 = data.result[0].property_value;
            }
            );
         //Info message 007
        this.oaoService.GetPropertyDetails('INFO_MESSAGE', 'INF_007')
            .subscribe(
            data => {
                this.inf_007 = data.result[0].property_value;
            }
            );
        this.oaoService.GetPropertyDetails('GENERIC_PROP', 'VALID_TO_MEDI')
            .subscribe(
            data => {
                this.max_year = data.result[0].property_value;
                console.log(this.max_year);
            }
            );

         this.oaoService.GetPropertyDetails('WARN_MESSAGE', 'WRN_002')
            .subscribe(
            data => {
                this.wrn_002 = data.result[0].property_value;
            }
            );

    }
    getAccno() {
        console.log("in acc")
        // this.model=this.oaoService.getData();
        this.model.skip = true;
        // this.onSubmit(this.model);
        this.model.app_id = this.application_id;

        console.log(this.model)
        this.oaoService.OAOCreateOrUpdateApplicant(this.model)
            .subscribe(
            data => {
                // this.oaoService.setData(data.Result);
                //  this.model=this.oaoService.getData();
                this.oaoService.GetApplicantsDetail(localStorage.getItem('application_id'))
                    .subscribe(
                    data => {
                        this.model = data.result[0];
                        //localStorage.clear();
                        jQuery('#success').modal('show');
                    });

            }
            );


    }

    updateSection() {
        this.oaoService.updatesection("section_3", localStorage.getItem('application_id')).subscribe(
            data => {
                console.log(data);
                console.log("updated");
                switch (this.prod_code) {

                    case 'EVR': this.router.navigate(['/TaxInformation']);
                        break;
                    case 'HML': this.router.navigate(['/Assets']);
                        break;
                    case 'PRL': this.router.navigate(['/Assets']);
                        break;
                    default: console.log("Page not found");

                }

            }
        );
    }
    dispDate(validto: any) {
        this.model.validTo = validto;
    }
    ngAfterViewInit() {

        var mon = this.date_v.getMonth() + 1;
        var year = this.date_v.getFullYear();
        this.model.validTo = this.date_v.getDate() + "/" + mon + "/" + year;
        var options = {
            format: "dd/mm/yyyy"
        }
        if (jQuery('.datepicker') && jQuery('.datepicker').length) {
            jQuery('.datepicker').dateDropper(options);
        }
        jQuery('body').on('change', '#validTo', function () {
            jQuery('#validTo').trigger('click');
        });
    }
    clear() {
        window.location.reload();
        localStorage.clear();
    }

    back(){
        this.isLoading=false;
    }

}