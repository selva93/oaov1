import { Component ,AfterViewInit,OnInit} from '@angular/core';
import { LoanDetailsPersonal } from "./../PersonalLoanRequirements/loandetailspersonal.component";
import {Router} from '@angular/router'
import { PersonalDetails } from "../personaldetailsinterface";
import { ConfigDetails } from "../configinterface";
import {OAOService} from "../OAO.Service"
import { google } from "../configinterface";
declare var jQuery:any;
declare var Ladda
@Component({
    selector: 'personaldetailscontact',
    templateUrl: './personaldetailscontact.component.html'
    
})
export class PersonaldetailsContactComponent implements AfterViewInit,OnInit{
    public application_id:any;
        model = new PersonalDetails('','','','','','','');
        private check:boolean=false;//to display modal
        private hold:boolean=false;
        public  state_drop: String[] = [];
        public  street: String[] = [];
        public showAddress:String="true"
        public showCustomAddr:String="true"
        public showCustomPAddr:String="true"
        public inf_001:String
        public wrn_001:String
        configMsg:ConfigDetails
        prod_type:string
        prod_code:string
       isLoading: boolean = false;
        public no_address_found_flag:string

        checkData:boolean=false; //chandan  //No changes in html page
        userDetailsObject; //chandan
        checkDupStatus:boolean=false; //chandan
        constructor(private oaoService:OAOService,private router: Router){
        this.checkData=this.oaoService.getHasData(); //chandan
                            // this.model=this.oaoService.getData();
                            // console.log(this.model)
                            // this.application_id=this.model.application_id
                            //call get method to get previous form value
                            // this.check=false;//to display modal
                            //   this.hold=false;
                            this.prod_type=localStorage.getItem('prod_type');
                            this.prod_code=localStorage.getItem('prod_code');
                           
                            this.oaoService.getConfig()   
                               .subscribe((data) => {  this.configMsg=JSON.parse(JSON.stringify(data.data));
                                                           }
                                   );

                                     this.application_id=localStorage.getItem('application_id');
                        
        }
                  onSubmit(formRecord:PersonalDetails){
                            this.isLoading = !this.isLoading;
                            formRecord.app_id=this.application_id;
                            formRecord.streetnum=this.model.streetnum;
                            formRecord.streetname=this.model.streetname;
                            formRecord.state=this.model.state;
                            formRecord.postcode=this.model.postcode;
                            formRecord.no_address_found_flag=this.no_address_found_flag;
                            if(this.no_address_found_flag=='Y'){
                                    formRecord.address=formRecord.housenum+" "+formRecord.streetnum+" "+formRecord.streetname+" "+formRecord.suburb+" "+formRecord.state+" "+formRecord.postcode;
                              }
                              if(formRecord.postal_home_address_flag==true){
                                        formRecord.pstreetnum=this.model.streetnum;
                                        formRecord.paddress=this.model.address;
                                        formRecord.pstreetname=this.model.streetname;
                                        formRecord.ppostcode=this.model.postcode;
                                        formRecord.pstate=this.model.state;
                                }
                            console.log(formRecord);
                     switch(this.prod_code){

                                case 'EVR':     this.oaoService.OAOCreateOrUpdateApplicant(formRecord)
                                                    .subscribe(
                                                        data => {
                                                    // this.oaoService.setData(data.Result);
                                                    this.check=true
                                                    if(!this.checkDupStatus)
                                                    {
                                                          if(this.hold==false)
                                                            {
                                                                this.showSave();
                                                            }
                                                        if(this.hold==true)
                                                        {
                                                        this.router.navigate(['/TaxInformation']);
                                                        }

                                                    }
                                        					
                                        		});
                                                break;
                                case 'HML':    this.oaoService.OAOCreateOrUpdateHomeloanApplicant(formRecord)
                                                     .subscribe(
                                                        data => {
                                                    // this.oaoService.setData(data.Result);
                                                    this.check=true
                                                     if(!this.checkDupStatus)
                                                    {
                                                        if(this.hold==false){
                                                        this.showSave();
                                                        }
                                                        if(this.hold==true){
                                                            this.router.navigate(['/PropertyDetails']);
                                                        }
                                                    }		
                                        		});
                                                break;
                                case 'PRL':    this.oaoService.OAOCreateOrUpdatePersonalloanApplicant(formRecord)
                                                     .subscribe(
                                                        data => {
                                                    // this.oaoService.setData(data.Result);
                                                    this.check=true
                                                     if(!this.checkDupStatus)
                                                    {
                                                        if(this.hold==false){
                                                        this.showSave();
                                                        }
                                                        if(this.hold==true){
                                                            this.router.navigate(['/LoanDetailsPersonal']);
                                                    }
                                                    }
                                        					
                                        		});
                                                break;
                                default:  console.log("Page not found");

            }
                    

                    }
                    // getAddress(str:google){
                    //     this.model.address=str.formatted_address;
                    // }
                    showCustomAddressFields(){
                            this.showCustomAddr="";
                            this.no_address_found_flag="Y";
                       }
                    showCustomPostalAddressFields(){
                            this.showCustomPAddr="";
                            this.no_address_found_flag="Y";
                    }
                    hideaddress(){
                        this.showCustomAddr="true";
                        this.no_address_found_flag="N";
                    }
                    hidePaddress(){
                        this.showCustomPAddr="true";
                        this.no_address_found_flag="N";
                    }
                   
                showSave(){
                if(this.check==true){
                    jQuery('#success').modal('show');
                   }
                }
          
                
                checkDup() //call service to check duplicate customer
                {
                    //chandan
                        if(!this.checkData)
                        {

                        console.log("else this.checkData="+this.checkData)
                        this.oaoService.checkDup(this.model)   
                        .subscribe(data => { 
                            this.checkDupStatus=data.status; 
                            console.log(data)
                            if(this.checkDupStatus){
                                 jQuery('#matching-customer-modal').modal('show');
                            }else{

                                
                                }

                        });
                    }
                }

               next(){

                    if(this.checkData)
                        {
                            console.log("if this.checkData="+this.checkData)
                            this.userDetailsObject=this.oaoService.getUserDeatils();
                            this.model.address=this.userDetailsObject.result.homeAddress;
                            this.model.paddress=this.userDetailsObject.result.postalAddress; 
                             }
                    
                    this.isLoading =false;
                    this.showAddress=""
                     this.hold=true;

                }

               

                   ngAfterViewInit(){
                            var saveFlag=false;
                            jQuery(".saveClose").click(function() {
                                    saveFlag=true;
                                   });
                   }
                   ngOnInit() {
       jQuery('input:visible:first').focus();
                        this.oaoService.GetPropertyDetails('INFO_MESSAGE','INF_001')
                                .subscribe(
                                    data =>{
                                         this.inf_001=data.result[0].property_value;
                                    }
                                );

                       this.oaoService.GetPropertyDetails('WARN_MESSAGE','WRN_001')
                                .subscribe(
                                    data =>{
                                         this.wrn_001=data.result[0].property_value;
                                    }
                                );
                        this.oaoService.GetPropertyDetails('commonCodes','STATE')
                                .subscribe(
                                            data =>{
                                                var count   =   Object.keys( data.result ).length;
                                                    for(var i = 0; i < count; i++){
                                                        this.state_drop.push(data.result[i].property_desc)
                                                    }
                                            }
                                );
                        this.oaoService.GetPropertyDetails('commonCodes','STREET_TYPE')
                                .subscribe(
                                            data =>{
                                                var count   =   Object.keys( data.result ).length;
                                                    for(var i = 0; i < count; i++){
                                                        this.street.push(data.result[i].property_desc)
                                                    }
                                            }
                                );
                                        //chandan
                                                if(this.checkData){
                                                    this.userDetailsObject=this.oaoService.getUserDeatils();
                                                    this.model.email=this.userDetailsObject.result.email;
                                                    this.model.mobile=this.userDetailsObject.result.mobile;
                                                    }
                                            //chandan
                                   this.oaoService.GetApplicantsDetail(localStorage.getItem('application_id'))
                                        .subscribe(
                                            data =>{
                                                 this.model=data.result[0];
                                            //chandan
                                                if(this.checkData){
                                                     this.userDetailsObject=this.oaoService.getUserDeatils();
                                                     this.model.email=this.userDetailsObject.result.email;
                                                     this.model.mobile=this.userDetailsObject.result.mobile;
                                                     jQuery('#email').attr('readonly', 'true');
                                                     jQuery('#mobile').attr('readonly', 'true'); 
                                                  }
                                            //chandan
                                                  this.model.postcode="1234";
                                                 this.model.ppostcode="1234";
                                                 if(this.model.address!=null || this.model.paddress!=null){
                                                        this.showAddress=""
                                                        this.hold=true;
                                                    }
                //for fb data
                if (this.oaoService.getFbData() == true) {
                    this.model = this.oaoService.getData();
                }
                                                 
                                                }
                                        )
                      }

         updateSection(){
            this.oaoService.updatesection("section_1",localStorage.getItem('application_id')).subscribe(
                                    data =>{
                                        console.log(data);
                                         console.log("updated");
                                          this.router.navigate(['/PersonalBasicInfo']);
                                    }
                                );
        }
		
		
		 laddaclose(){
            this.isLoading=false;
        }
    
}