import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { PersonalDetails } from "../personaldetailsinterface";
import { ConfigDetails } from "../configinterface";
import { OAOService } from "../OAO.Service"
declare var jQuery: any;
declare var Ladda
@Component({
    selector: 'taxinfo',
    templateUrl: './taxinfo.html'

})
export class TaxInformation implements OnInit {
    tfnval = '';
    reasonval = '';
    public items: any[] = [];
    application_id: any;
    model = new PersonalDetails('', '', '', '', '', '', '');
    private check: boolean = false;
    private hold: boolean = false;
    public idCheck_v: String;
    public wrn_002: String;
    public wrn_003: String;
    public inf_002: String;
    public inf_003: String;
    public skip: boolean = false;
    public section_2: Boolean;
    prod_type: string
    err: string;
    prod_code: string;
    configMsg: ConfigDetails
    NA: string;
     isLoading: boolean = false;
    checkData:boolean=false; //chandan
    userDetailsObject; //chandan
    constructor(private oaoService: OAOService, private router: Router)
     {
          this.checkData=this.oaoService.getHasData(); //chandan
        //  this.model=this.oaoService.getData();
        this.NA = null;
        this.prod_type = localStorage.getItem('prod_type');
        this.prod_code = localStorage.getItem('prod_code');
        this.application_id = localStorage.getItem('application_id');
        this.oaoService.getConfig()
            .subscribe((data) => {
            this.configMsg = JSON.parse(JSON.stringify(data.data));
            }
            );
        //   this.application_id=this.model.application_id
    }

    tfnfunc(event: any) { // without type info
        this.tfnval = event.target.value;
        // this.model.exemption = '0'
        // this.err = "";
    }

    reasonfunc(event: any) { // without type info
        if (event.target.value == '0') {
            this.reasonval = '';
            // this.err = "";
            // this.model.exemption = '0'
        }
        else {
            this.reasonval = event.target.value;
            // this.model.exemption = '0'
            // this.err = "";
        }

    }
    clear() {
       // window.location.reload();
        this.oaoService.setFindLogin(false);//chandan
        this.oaoService.setHasData(false);//chandan
        console.log(this.oaoService.getFindLogin())//chandan
        localStorage.clear();
    }
    onSubmit(formRecord: PersonalDetails) {
         this.isLoading=!this.isLoading;
        formRecord.app_id = this.application_id;
        if (this.model.skip == false) {
            formRecord.skip = false;
        } else {
            formRecord.skip = true;
        }
        console.log(formRecord);
        if ((formRecord.tfn == null || formRecord.tfn == "") && formRecord.exemption == '0') {
            this.err = "err";
            return;
        } else {
            this.oaoService.OAOCreateOrUpdateApplicant(formRecord)
                .subscribe(
                data => {
                    // this.oaoService.setData(data.Result);
                    this.check = true;
                    if (this.idCheck_v == "O") 
                    {
                //chandan
                        if(this.checkData)
                        { 
                            this.getAccno();
                        }else
                        {
                             this.showSave();
                        } 
                 //chandan  
                    } else if (this.idCheck_v == "M") {
                        this.router.navigate(['/OnlineIdCheck']);
                    } else {
                        this.getAccno();
                    }
                }

                );
        }

    }


    showSave() {
        if (this.check == true) {
            jQuery('#onlineid-check').modal('show');
        }
    }
    updateSection() {
        this.oaoService.updatesection("section_2", localStorage.getItem('application_id')).subscribe(
            data => {
                console.log(data);
                console.log("updated");
                this.router.navigate(['/PersonalContactInfo']);
            }
        );
    }

    ngOnInit() {
		jQuery('input:visible:first').focus();
        jQuery('#mlogin').hide();//chandan
        this.oaoService.GetPropertyDetails('commonCodes', 'EXRSN')
            .subscribe(
            data => {
                var count = Object.keys(data.result).length;
                for (var i = 0; i < count; i++) {
                    this.items.push({
                        prop_desc: data.result[i].property_desc,
                        prop_val: data.result[i].property_value
                    })
                }
            }
            );
        this.oaoService.GetApplicantsDetail(localStorage.getItem('application_id'))
            .subscribe(
            data => {
                this.model = data.result[0];
                this.model.skip = this.skip;
        //chandan
            if(this.checkData){
                  jQuery('#reb').hide();//chandan
                  jQuery('#mlogin').show();//chandan
                this.userDetailsObject=this.oaoService.getUserDeatils();
                this.model.tfn=this.userDetailsObject.result.TFN
                this.model.exemption=this.userDetailsObject.result.exemptionReason; 
                console.log(this.model.tfn);
                if((this.model.tfn)==null ||(this.model.tfn)!="" ){
                      jQuery('#tfn').attr('readonly', 'true');
                      jQuery('#exemption').prop('disabled', true);
                }
               
            }
        //chandan
                if (this.model.exemption == null) {
                    this.NA = null;
                    this.model.exemption = '0'
                }
                console.log(this.model)
            }
            )

        //for idCheck
        this.oaoService.GetPropertyDetails('turnOnOff', 'idCheck')
            .subscribe(
            data => {
                this.idCheck_v = data.result[0].property_value;
                console.log(data)
            }
            );
        //warning message 002
        this.oaoService.GetPropertyDetails('WARN_MESSAGE', 'WRN_002')
            .subscribe(
            data => {
                this.wrn_002 = data.result[0].property_value;
            }
            );
        //warning message 003
        this.oaoService.GetPropertyDetails('WARN_MESSAGE', 'WRN_003')
            .subscribe(
            data => {
                this.wrn_003 = data.result[0].property_value;
            }
            );
        //Info message 002
        this.oaoService.GetPropertyDetails('INFO_MESSAGE', 'INF_002')
            .subscribe(
            data => {
                this.inf_002 = data.result[0].property_value;
            }
            );
             this.oaoService.GetPropertyDetails('INFO_MESSAGE', 'INF_003')
            .subscribe(
            data => {
                this.inf_003 = data.result[0].property_value;
            }
            );
    }

    getAccno() {
        // this.model=this.oaoService.getData();
        this.model.skip = true;
        // this.onSubmit(this.model);
        this.model.app_id = this.application_id;

        this.oaoService.OAOCreateOrUpdateApplicant(this.model)
            .subscribe(
            data => {
                // this.oaoService.setData(data.Result);
                // this.model=this.oaoService.getData();
                this.oaoService.GetApplicantsDetail(localStorage.getItem('application_id'))
                    .subscribe(
                    data => {
                        this.model = data.result[0];
                        //localStorage.clear(); //chandan
                        if (this.idCheck_v == "O") {
                            jQuery('#success').modal('show');
                        }
                        if (this.idCheck_v == "N") {
                            jQuery('#success').modal('show');
                        }
                    }
                    );


            }

            );


    }

}