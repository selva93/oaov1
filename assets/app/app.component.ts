import { Component ,AfterViewInit,OnInit} from '@angular/core';
import {Router} from '@angular/router'
import { PersonalDetails } from "./personaldetailsinterface";
import {checkbox} from './checkboxinterface';
import { ConfigDetails } from "./configinterface";
import {OAOService} from "./OAO.Service"
import { UserDetailsObject } from "./LoginDetails/UserDetailsInterface"; //chandan
import { FacebookService, LoginResponse, LoginOptions, UIResponse, UIParams } from 'ng2-facebook-sdk';

declare var google:any;
declare var googleLoaded:any;

declare var jQuery:any;
declare var moment: any;

@Component({
    selector: 'home',
    templateUrl: './app.component.html'
})
export class AppComponent {
  modal = new checkbox(false,false);
  prod:string;
  dis_v:boolean=false;
  img:string=localStorage.getItem('prod_code');

 FbData:boolean=false;
  checkData:boolean=false; //chandan
  userDetailsObject; //chandan
  title;//chandan
  fName; //chandan
  age;//int
   fbdata= new PersonalDetails('', '', '', '', '', '', '');
  constructor(private oaoService:OAOService,private router: Router,private fb: FacebookService){
        console.log('Initializing Facebook');

    fb.init({
      appId: '658955644261049',
      version: 'v2.8'
    });
  }
//chandan
 
        //facebook login

          private handleError(error) {
    console.error('Error processing action', error);
  }


login() {
    this.fb.login()
      .then((res: LoginResponse) => {
        console.log('Logged in', res);
         //to get profile data
         this.fb.api('/me','get', {fields:['first_name','last_name','birthday','id','email','location']})
      .then((res: any) => {
        console.log('Got the users profile', res);
        this.oaoService.setFbData(true);
        this.processFBdata(res);
      })
      .catch(this.handleError);
      })
      .catch(this.handleError);

      
  }
   processFBdata(data)
  {    
                  if(data.first_name==null){}else{this.fbdata.fname=data.first_name;}
                  if(data.last_name==null){}else{this.fbdata.lname=data.last_name;}
                  if(data.email==null){}else{this.fbdata.email=data.email;}
                  if(data.birthday==null){}else{this.fbdata.dob=data.birthday;}
                  if(data.location==null){}else{this.fbdata.address=data.location;}
                 this.oaoService.setData(this.fbdata);
                 this.FbData=this.oaoService.getFbData();
                    this.modal.aus_citizen=true;
                    this.modal.age_test=true;
                    jQuery('#savingsaccount-modal').modal('show');
                 
            
  }



 ngOnInit() //to active model and hide the buttons  while coming from login page
 {
        console.log("appComponent ngOnInit()")
        this.checkData=this.oaoService.getHasData();
        if(this.checkData)
        {
            this.GetUserDetails(); //get the all details of user by sending user name\
            
        }
  }

  FindLogin()
  {
      this.oaoService.setFindLogin(true);
      //localStorage.setItem('findLogin',"true");

  }

  GetUserDetails()
  {
      console.log("GetUserDetails")
       this.oaoService.GetLoginUserDetails(this.oaoService.getUserDeatils()).subscribe(
            data => {
                 this.oaoService.setUserDeatils(data);
                this.userDetailsObject=this.oaoService.getUserDeatils();
                console.log(this.userDetailsObject);
            
                var str=this.userDetailsObject.result.fName
                str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                 return letter.toUpperCase();
                    });
                this.fName=str;
                if(parseInt(this.userDetailsObject.result.age)<18)
                {
                    jQuery('#not_eligible-modal').modal('show');
                }
                else
                {  
                    this.modal.aus_citizen=true;
                    this.modal.age_test=true;
                    jQuery('#savingsaccount-modal').modal('show');
                } 
            });
  }

//chandan 

    setModalType(type_v:string){
        console.log("setModalType()")
       
        console.log(this.img);
        localStorage.setItem('prod_code',type_v); //chandan
        //this.prod=type_v; //chandan
         this.img=localStorage.getItem('prod_code');

        jQuery('#savingsaccount-modal').modal('show');
        
    }
    

    setProdType(s_j:string){
        console.log("setProdType()");
        this.prod = localStorage.getItem('prod_code');
         this.oaoService.GetPropertyDetails('commonCodes','PRODUCT_TYPE')
                                .subscribe(
                                            data =>{
                                                var count   =   Object.keys( data.result ).length;
                                                    for(var i = 0; i < count; i++){
                                                        if(data.result[i].property_value==this.prod){
                                                            console.log(data.result[i].property_value==this.prod)
                                                            // this.modal.age_test=false;
                                                            // this.modal.aus_citizen=false;
                                                            localStorage.setItem('prod_type', data.result[i].property_desc);
                         //chandan commented             localStorage.setItem('prod_code', this.prod);
                                                            localStorage.setItem('singleORjoint', s_j);
                                                            if(s_j=='single'){
                                                            this.router.navigate(['/PersonalBasicInfo']);
                                                            }else{
                                                                this.router.navigate(['/PersonalBasicInfo']);
                                                            }
                                                        }
                                                    }
                                            }
                                );
    }
    setFalse(){
        this.modal.age_test=false;
        this.modal.aus_citizen=false;
        this.dis_v=true;
        this.logout(); //chandan
         this.checkData=false;//chandan
    }
//chandan
    logout()
    {
        if(this.checkData){
             this.oaoService.setHasData(false); 
            this.oaoService.logout().subscribe(
            data => { console.log(data);});
        }
       
    }
//chandan
}