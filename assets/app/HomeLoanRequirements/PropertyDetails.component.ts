import { Component ,AfterViewInit,OnInit} from '@angular/core';
import {Router} from '@angular/router';
import { ConfigDetails } from "../configinterface";
import {OAOService} from "../OAO.Service"
import { PersonalDetails } from "../personaldetailsinterface";
import { Common } from "../commonFunc";
 declare var jQuery:any;
// declare var Ladda:any;
@Component({
    selector: 'propertydetails',
    templateUrl: './propertydetails.component.html'
    
})
export class PropertyDetails implements OnInit{
    public application_id:any;
    prod_type:string
    prod_code:string
    configMsg:ConfigDetails
    model = new PersonalDetails('','','','','','','')
    testmodel = new PersonalDetails('','','','','','','')
    public  propType: any[] = [];
     isLoading: boolean = false;
     constructor(private oaoService:OAOService,private router: Router){
         this.prod_type=localStorage.getItem('prod_type');
         this.prod_code=localStorage.getItem('prod_code');
         this.application_id=localStorage.getItem('application_id');
         
         
     }
     ngOnInit(){
       jQuery('input:visible:first').focus();
         this.oaoService.GetApplicantsDetail(localStorage.getItem('application_id'))
                                        .subscribe(
                                            data =>{
                                                 this.model=data.result[0];
                                                 this.testmodel=this.model;
                                                 if(this.model.loantype==null){
                                                     this.model.loantype="REFINANCE";
                                                 }
                                                 if( this.model.ownership==null){
                                                      this.model.ownership="OWNER OCCUPIER";
                                                 }
                                                 if( this.model.proptype==null){
                                                      this.model.proptype='0';
                                                 }
                                            }
                                        );
         this.oaoService.getConfig()   
           .subscribe((data) => {  this.configMsg=JSON.parse(JSON.stringify(data.data));
                                   console.log(this.configMsg)}); 

         this.oaoService.GetPropertyDetails('commonCodes','PROP_TYPE')
            .subscribe(
                data =>{
                    var count   =   Object.keys( data.result ).length;
                        for(var i = 0; i < count; i++){
                            this.propType.push({
                                prop_desc: data.result[i].property_desc,
                                prop_val: data.result[i].property_value
                            })
                        }
                }
            );
     }
    
     onSubmit(formRecord:PersonalDetails){
                            this.isLoading=!this.isLoading;
                            formRecord.app_id=this.application_id;
                            // formRecord.payoutbal=this.testmodel.payoutbal
                            // formRecord.purchaseprice=this.testmodel.purchaseprice
                            // formRecord.rentalincome=this.testmodel.rentalincome
                            if((!String(formRecord.payoutbal).match(/\./g) || String(formRecord.payoutbal).match(/\./g)) && String(formRecord.payoutbal).match(/\,/g) ){
                            var payoutbal = formRecord.payoutbal.replace(/\,/g,"");
                            formRecord.payoutbal=payoutbal;
                         }
                         if((!String(formRecord.rentalincome).match(/\./g) || String(formRecord.rentalincome).match(/\./g)) && String(formRecord.rentalincome).match(/\,/g) ){
                            var rentalincome = formRecord.rentalincome.replace(/\,/g,"");
                         formRecord.rentalincome=rentalincome;
                        }
                           if((!String(formRecord.purchaseprice).match(/\./g) || String(formRecord.purchaseprice).match(/\./g)) && String(formRecord.purchaseprice).match(/\,/g) ){
                            var purchaseprice = formRecord.purchaseprice.replace(/\,/g,"");
                            formRecord.purchaseprice=purchaseprice;
                           }
                           
                            
                            console.log("updated"+formRecord);
                            console.log(formRecord);
                            this.oaoService.OAOCreateOrUpdateHomeloanApplicant(formRecord)
                                .subscribe(
                                    data => {
                                    console.log("sample"+data);
                                     this.router.navigate(['/LoanDetails']);
                    				}
                                 );

                            
      }
      updateSection(){
            this.oaoService.updatesection("section_1",localStorage.getItem('application_id')).subscribe(
                                    data =>{
                                        console.log(data);
                                         console.log("updated");
                                          this.router.navigate(['/PersonalContactInfo']);
                                    }
                                );
        }
    
    clear(radio_var:any){
        switch(radio_var){
            case 'REFINANCE':     this.model.property='';
                                break;
            case 'NEW PURCHASE':this.model.property='Yes';  
                                this.model.payoutbal='';
                                break;
            case 'OWNEROCCUPIER':this.model.rentalincome=null;
                                    break;
        }
    }
    AmountFormatter(amountvalue: any, var_v: any) {
        if( typeof amountvalue != 'undefined' && amountvalue!=null && amountvalue!=''  ){
            console.log("asd "+amountvalue+" "+var_v)
            var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'AUD',
            minimumFractionDigits: 2,
        });
        //     this.testmodel[var_v]="";
        //  this.testmodel[var_v]=amountvalue;
        var finalString = formatter.format(amountvalue);
		finalString = finalString.replace('A$','');
        this.model[var_v] = finalString.replace('$','');
    }else{
         this.model[var_v]="0.0";
    }
    }

   revert(oldvalue:any,var_v: any){
        var tmpOldvalue;
       if(oldvalue!=null && String(oldvalue).match(/\,/g)){
        tmpOldvalue=oldvalue.replace(/\,/g,'');
        console.log(tmpOldvalue);
        this.model[var_v]=tmpOldvalue.substr(0,tmpOldvalue.length-3);
        console.log(this.model[var_v]);
        }
    }

}