import { Component, AfterViewInit, OnInit } from '@angular/core';
import { PropertyDetails } from "./PropertyDetails.component";
import { LoanSummary } from "./loansummary.component";
import { Router } from '@angular/router';
import { ConfigDetails } from "../configinterface";
import { OAOService } from "../OAO.Service"
import { PersonalDetails } from "../personaldetailsinterface";
 declare var jQuery:any;
// declare var Ladda:any;
@Component({
    selector: 'loandetails',
    templateUrl: './loandetails.component.html'

})
export class LoanDetails implements OnInit {
    model = new PersonalDetails('', '', '', '', '', '', '');
    public application_id: any;
    prod_type: string
    prod_code: string
    configMsg: ConfigDetails;
    public loanTerm: any[] = [];
    public repaymentType: any[] = [];
    isLoading:Boolean=false;
   
    constructor(private oaoService: OAOService, private router: Router) {
        this.prod_type = localStorage.getItem('prod_type');
        this.prod_code = localStorage.getItem('prod_code');
        this.application_id = localStorage.getItem('application_id');
        this.oaoService.getConfig()
            .subscribe((data) => { this.configMsg = JSON.parse(JSON.stringify(data.data)); });

    }
    ngOnInit() {
        jQuery('input:visible:first').focus();
        this.oaoService.GetApplicantsDetail(localStorage.getItem('application_id'))
            .subscribe(
            data => {
                this.model = data.result[0];
                if (this.model.frequencyType == null || this.model.interesttype == null) {
                    this.model.frequencyType = "Monthly";
                    this.model.interesttype = "FIXED";
                }
                if (this.model.loanterm == null) {
                    this.model.loanterm = '0';
                }
                if (this.model.repaymenttype == null) {
                    this.model.repaymenttype = '0';
                }

            }
            );
        this.oaoService.GetPropertyDetails('commonCodes', 'LOAN_TERM')
            .subscribe(
            data => {
                var count = Object.keys(data.result).length;
                for (var i = 0; i < count; i++) {
                    this.loanTerm.push({
                        prop_desc: data.result[i].property_desc,
                        prop_val: data.result[i].property_value
                    })
                }
            }
            );
        this.oaoService.GetPropertyDetails('commonCodes', 'REPAYMENT_TYPE')
            .subscribe(
            data => {
                var count = Object.keys(data.result).length;
                for (var i = 0; i < count; i++) {
                    this.repaymentType.push({
                        prop_desc: data.result[i].property_desc,
                        prop_val: data.result[i].property_value
                    })
                }
            }
            );
    }

    onSubmit(formRecord: PersonalDetails) {
        this.isLoading=!this.isLoading;
        formRecord.app_id = this.application_id;
        if ((!String(formRecord.amtborrow).match(/\./g) || String(formRecord.amtborrow).match(/\./g)) && String(formRecord.amtborrow).match(/\,/g)) {
            var amtborrow = formRecord.amtborrow.replace(/\,/g, "");
            formRecord.amtborrow = amtborrow;
        }
        if (formRecord.consolidateMortage == true) {
            if ((!String(formRecord.estvalue).match(/\./g) || String(formRecord.estvalue).match(/\./g)) && String(formRecord.estvalue).match(/\,/g)) {
                var estvalue = formRecord.estvalue.replace(/\,/g, "");
                formRecord.estvalue = estvalue;
            }
        }
        if (formRecord.consolidateotherMortage == true) {
            if ((!String(formRecord.cc_estvalue).match(/\./g) || String(formRecord.cc_estvalue).match(/\./g)) && String(formRecord.cc_estvalue).match(/\,/g)) {
                var cc_estvalue = formRecord.cc_estvalue.replace(/\,/g, "");
                formRecord.cc_estvalue = cc_estvalue;
            }
            if ((!String(formRecord.pl_estvalue).match(/\./g) || String(formRecord.pl_estvalue).match(/\./g)) && String(formRecord.pl_estvalue).match(/\,/g)) {
                var pl_estvalue = formRecord.pl_estvalue.replace(/\,/g, "");
                formRecord.pl_estvalue = pl_estvalue;
            }
            if ((!String(formRecord.cl_estvalue).match(/\./g) || String(formRecord.cl_estvalue).match(/\./g)) && String(formRecord.cl_estvalue).match(/\,/g)) {
                var cl_estvalue = formRecord.cl_estvalue.replace(/\,/g, "");
                formRecord.cl_estvalue = cl_estvalue;
            }
            if ((!String(formRecord.sl_estvalue).match(/\./g) || String(formRecord.sl_estvalue).match(/\./g)) && String(formRecord.sl_estvalue).match(/\,/g)) {
                var sl_estvalue = formRecord.sl_estvalue.replace(/\,/g, "");
                formRecord.sl_estvalue = sl_estvalue;
            }
            if ((!String(formRecord.o_estvalue).match(/\./g) || String(formRecord.o_estvalue).match(/\./g)) && String(formRecord.o_estvalue).match(/\,/g)) {
                var o_estvalue = formRecord.o_estvalue.replace(/\,/g, "");
                formRecord.o_estvalue = o_estvalue;
            }

        }


        console.log(formRecord);
        this.oaoService.OAOCreateOrUpdateHomeloanApplicant(formRecord)
            .subscribe(
            data => {
                console.log("sample" + data);
                this.router.navigate(['/LoanSummary']);
            }
            );

    }
    updateSection() {
        this.oaoService.updatesection("section_2", localStorage.getItem('application_id')).subscribe(
            data => {
                console.log(data);
                console.log("updated");
                this.router.navigate(['/PropertyDetails']);
            }
        );
    }
    percentage_var: any
    checkPercentage() {

        this.percentage_var = 100 - parseInt(this.model.fixedper);
        this.model.variableper = this.percentage_var;

    }
    clear(radio_var: any) {
        switch (radio_var) {
            case 'FIXED': this.model.fixedper = '';
                this.model.variableper = ''
                break;
            case 'VARIABLE': this.model.fixedper = '';
                this.model.variableper = ''
                break;
        }
    }
    clearCheckbox(checkbox_var: any, cond: boolean) {
        switch (checkbox_var) {
            case 'consolidateMortage': if (cond == false) {
                this.model.estvalue = '';
                this.model.finInstitution = '';
                this.model.propaddress_m = '';
            }
                break;
            case 'consolidateotherMortage':
                break;
            default: console.log("ewfjk")
        }
    }
    AmountFormatter(amountvalue: any, var_v: any) {
     if( typeof amountvalue != 'undefined' && amountvalue!=null && amountvalue!=''  ){
            console.log("asd "+amountvalue+" "+var_v)
            var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'AUD',
            minimumFractionDigits: 2,
        });
        //     this.testmodel[var_v]="";
        //  this.testmodel[var_v]=amountvalue;
        var finalString = formatter.format(amountvalue);
		finalString = finalString.replace('A$','');
        this.model[var_v] = finalString.replace('$','');
    }else{
         this.model[var_v]="0.0";
    }
    }

    revert(oldvalue: any, var_v: any) {
        var tmpOldvalue;
        if (oldvalue != null && String(oldvalue).match(/\,/g)) {
            tmpOldvalue = oldvalue.replace(/\,/g, '');
            console.log(tmpOldvalue);
            this.model[var_v] = tmpOldvalue.substr(0, tmpOldvalue.length - 3);
            console.log(this.model[var_v]);
        }
    }



}