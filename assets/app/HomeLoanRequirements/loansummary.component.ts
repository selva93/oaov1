import { Component ,AfterViewInit,OnInit} from '@angular/core';
import { LoanDetails } from "./loandetails.component";
import {Router} from '@angular/router';
import { ConfigDetails } from "../configinterface";
import {OAOService} from "../OAO.Service"
import { PersonalDetails } from "../personaldetailsinterface";
// declare var jQuery:any;
// declare var Ladda:any;
@Component({
    selector: 'loansummary',
    templateUrl: './loansummary.component.html'
    
})
export class LoanSummary {
    model = new PersonalDetails('', '', '', '', '', '', '');
    public application_id: any;
    prod_type: string
    prod_code: string
    isLoading: Boolean = false;
    public emiamount: any;
    public months: any;
    public repaymentamount: string;
    public amtbrrow: string;
    public estvalue_v
    public cc_estvalue_v
    public pl_estvalue_v
    public cl_estvalue_v
    public sl_estvalue_v
    public o_estvalue_v
    constructor(private oaoService: OAOService, private router: Router) {
        this.prod_type = localStorage.getItem('prod_type');
        this.prod_code = localStorage.getItem('prod_code');
        this.application_id = localStorage.getItem('application_id');

    }
    ngOnInit() {
        this.oaoService.GetApplicantsDetail(localStorage.getItem('application_id'))
            .subscribe(
            data => {
                this.model = data.result[0];
                this.emi();
                var formatter = new Intl.NumberFormat('en-US', {
                    style: 'currency',
                    currency: 'AUD',
                    minimumFractionDigits: 2,
                });
                if (this.model.estvalue == null)
                { this.estvalue_v = 0 }
                else { this.estvalue_v = parseInt(this.model.estvalue) }
                if (this.model.cc_estvalue == null)
                { this.cc_estvalue_v = 0 }
                else { this.cc_estvalue_v = parseInt(this.model.cc_estvalue) }
                if (this.model.pl_estvalue == null)
                { this.pl_estvalue_v = 0 }
                else { this.pl_estvalue_v = parseInt(this.model.pl_estvalue) }
                if (this.model.cl_estvalue == null)
                { this.cl_estvalue_v = 0 }
                else { this.cl_estvalue_v = parseInt(this.model.cl_estvalue) }
                if (this.model.sl_estvalue == null)
                { this.sl_estvalue_v = 0 }
                else { this.sl_estvalue_v = parseInt(this.model.sl_estvalue) }
                if (this.model.o_estvalue == null)
                { this.o_estvalue_v = 0 }
                else { this.o_estvalue_v = parseInt(this.model.o_estvalue) }
                this.amtbrrow = formatter.format(parseInt(this.model.amtborrow+this.estvalue_v+this.cc_estvalue_v+this. pl_estvalue_v +this.cl_estvalue_v +this.sl_estvalue_v+this. o_estvalue_v));
            }
            );

    }

    onSubmit() {
        this.isLoading = !this.isLoading;
        this.router.navigate(['/IncomeExpense']);
    }
     updateSection(){
            this.oaoService.updatesection("section_2",localStorage.getItem('application_id')).subscribe(
                                    data =>{
                                        console.log(data);
                                         console.log("updated");
                                          this.router.navigate(['/LoanDetails']);
                                    }
                                );
        }

        emi(){
           this.emiamount= this.model.amtborrow;
            this.months= parseInt(this.model.loanterm) *12;
            var r=3.89/(12*100);
            // this.repaymentamount = Math.floor((this.emiamount * r *Math.pow((1+r),this.months))/(Math.pow((1+r),this.months)-1));
             var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'AUD',
            minimumFractionDigits: 2,
        });
        this.repaymentamount = formatter.format( Math.floor((this.emiamount * r *Math.pow((1+r),this.months))/(Math.pow((1+r),this.months)-1)));
            console.log(this.repaymentamount)
        }
}