import { Component, AfterViewInit, OnInit } from '@angular/core';
import { LoanDetailsPersonal } from "./../PersonalLoanRequirements/loandetailspersonal.component";
import { AssetsComponent } from "./assets.component";
import { LoanSummary } from "./loansummary.component";
import { Router } from '@angular/router';
import { ConfigDetails } from "../configinterface";
import { OAOService } from "../OAO.Service"
import { PersonalDetails } from "../personaldetailsinterface";
declare var jQuery: any;
// declare var Ladda:any;
@Component({
    selector: 'incomeexpense',
    templateUrl: './incomeexpense.component.html'

})
export class IncomeExpense {

    model = new PersonalDetails('', '', '', '', '', '', '');
    public application_id: any;
    prod_type: string
    prod_code: string
    configMsg: ConfigDetails;
    isLoading:Boolean=false;
    constructor(private oaoService: OAOService, private router: Router) {
        this.prod_type = localStorage.getItem('prod_type');
        this.prod_code = localStorage.getItem('prod_code');
        this.application_id = localStorage.getItem('application_id');
        this.oaoService.getConfig()
            .subscribe((data) => { this.configMsg = JSON.parse(JSON.stringify(data.data)); });

    }
    ngOnInit() {
        jQuery('input:visible:first').focus();
        this.oaoService.GetApplicantsDetail(localStorage.getItem('application_id'))
            .subscribe(
            data => {
                this.model = data.result[0];
                if (this.model.employed == null) {
                    this.model.employed = "EMPLOYED";
                }
            }
            );

    }

    onSubmit(formRecord: PersonalDetails) {
        this.isLoading=!this.isLoading;
        formRecord.app_id = this.application_id;
        formRecord.skip = false;
         if ((!String(formRecord.earnPerMonth).match(/\./g) || String(formRecord.earnPerMonth).match(/\./g)) && String(formRecord.earnPerMonth).match(/\,/g)) {
            var earnPerMonth = formRecord.earnPerMonth.replace(/\,/g, "");
            formRecord.earnPerMonth = earnPerMonth;
         }
          if ((!String(formRecord.monthlyLivingExpenses).match(/\./g) || String(formRecord.monthlyLivingExpenses).match(/\./g)) && String(formRecord.monthlyLivingExpenses).match(/\,/g)) {
            var monthlyLivingExpenses = formRecord.monthlyLivingExpenses.replace(/\,/g, "");
            formRecord.monthlyLivingExpenses = monthlyLivingExpenses;
         }
        
        
        switch (this.prod_code) {
            case 'HML': this.oaoService.OAOCreateOrUpdateHomeloanApplicant(formRecord)
                .subscribe(
                data => {
                    console.log("sample" + data);
                    this.router.navigate(['/Assets']);
                }
                );
                break;
            case 'PRL': this.oaoService.OAOCreateOrUpdatePersonalloanApplicant(formRecord)
                .subscribe(
                data => {
                    console.log("sample" + data);
                    this.router.navigate(['/Assets']);
                }
                );
                break;
            default: console.log("Page not found");

        }



    }


    updateSection() {
        this.oaoService.updatesection("section_2", localStorage.getItem('application_id')).subscribe(
            data => {
                console.log(data);
                console.log("updated");
                switch (this.prod_code) {

                    case 'HML': this.router.navigate(['/LoanSummary']);
                        break;
                    case 'PRL': this.router.navigate(['/LoanDetailsPersonal']);
                        break;
                    default: console.log("Page not found");

                }
            }
        );
    }
    AmountFormatter(amountvalue: any, var_v: any) {
        if( typeof amountvalue != 'undefined' && amountvalue!=null && amountvalue!=''  ){
            console.log("asd "+amountvalue+" "+var_v)
            var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'AUD',
            minimumFractionDigits: 2,
        });
        //     this.testmodel[var_v]="";
        //  this.testmodel[var_v]=amountvalue;
        var finalString = formatter.format(amountvalue);
		finalString = finalString.replace('A$','');
        this.model[var_v] = finalString.replace('$','');
    }else{
         this.model[var_v]="0.0";
    }
    }

    revert(oldvalue: any, var_v: any) {
        var tmpOldvalue;
        if (oldvalue != null && String(oldvalue).match(/\,/g)) {
            tmpOldvalue = oldvalue.replace(/\,/g, '');
            console.log(tmpOldvalue);
            this.model[var_v] = tmpOldvalue.substr(0, tmpOldvalue.length - 3);
            console.log(this.model[var_v]);
        }
    }

}