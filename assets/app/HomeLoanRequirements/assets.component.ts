import { Component, OnInit } from '@angular/core';
import { IncomeExpense } from "./incomeexpense.component";
import { ConfigDetails } from "../configinterface";
import { OAOService } from "../OAO.Service"
import { PersonalDetails } from "../personaldetailsinterface";
import { Assetdetails } from "../assetsInterface";
import { Router } from '@angular/router';
declare var jQuery: any;

@Component({
    selector: 'selector',
    templateUrl: 'assets.html'
})
export class AssetsComponent implements OnInit {
    model = new PersonalDetails('', '', '', '', '', '', '');
    Assets: Assetdetails;

    public MaxLimit: Number;
    public assetsLength: Number;
    public LiabilitiesLength: Number;
    public assetsDetails: any;
    public LiabilitiesArray: any;

    public assetType: any[] = [];
    public liabilityType: any[] = [];
    public freqType: any[] = [];

    public application_id: any;
    public wrn_002: String;
    public inf_004: String;
    configMsg: ConfigDetails
    prod_type: string;
    prod_code: string;
    public idCheck_v: String;
    public inf_loan: string;
    finalSend: PersonalDetails;
    isLoading:Boolean=false;
    checkData:boolean=false; //chandan 
    constructor(private oaoService: OAOService, private router: Router) {
        this.checkData=this.oaoService.getHasData(); //chandan
        this.assetsDetails = [];
        this.LiabilitiesArray = [];
        this.prod_type = localStorage.getItem('prod_type');
        this.prod_code = localStorage.getItem('prod_code');
        this.application_id = localStorage.getItem('application_id');
        this.model.assettype = "0";
        this.model.Liabilitiestype = "0";
        //for idCheck
        this.oaoService.GetPropertyDetails('turnOnOff', 'idCheck')
            .subscribe(
            data => {
                this.idCheck_v = data.result[0].property_value;
                console.log(data)
            }
            );
        //warning message 002
        this.oaoService.GetPropertyDetails('WARN_MESSAGE', 'WRN_002')
            .subscribe(
            data => {
                this.wrn_002 = data.result[0].property_value;
            }
            );
        this.oaoService.GetApplicantsDetail(localStorage.getItem('application_id'))
            .subscribe(
            data => {
                this.model = data.result[0];
                this.LiabilitiesArray = this.model.Liabilities
                this.assetsDetails = this.model.assets
                if (this.model.assettype == null) {
                    this.model.assettype = '0'
                }
                if (this.model.Liabilitiestype == null) {
                    this.model.Liabilitiestype = '0'
                }
                if (this.model.Payment_Frequency == null) {
                    this.model.Payment_Frequency = '0'
                }

            }
            );
        // this.Assets = new Assetdetails('','');


        this.oaoService.GetPropertyDetails('GENERIC_PROP', 'ASSET_LIABILITY_MAX')
            .subscribe(
            data => {
                //console.log(data.result[0].property_value);
                this.MaxLimit = data.result[0].property_value;
            }
            );
    }



    onSubmit(formRecord: PersonalDetails) {
       
        var assets = {
            'assettype': formRecord.assettype,
            'assetvalue': formRecord.assetvalue
        };
        var assetvalue = formRecord.assetvalue.replace(/\,/g, "");
        formRecord.assetvalue = assetvalue;
        if (this.assetsLength <= this.MaxLimit) {
            this.assetsDetails.push(assets);
        }
        this.assetsLength = this.assetsDetails.length;
        this.model.assettype="0";
        this.model.assetvalue=null;

    }
    onSubmitLiabilitiesDetails(formRecord: PersonalDetails) {
        var Liabilities = {
            'Liabilitiestype': formRecord.Liabilitiestype,
            'Payable_Amount': formRecord.Payable_Amount,
            'Payment_Frequency': formRecord.Payment_Frequency,
            'Balance_Pending': formRecord.Balance_Pending,
            'Financial_Institution': formRecord.Financial_Institution
        };
        var Payable_Amount = formRecord.Payable_Amount.replace(/\,/g, "");
        formRecord.Payable_Amount = Payable_Amount;
        var Balance_Pending = formRecord.Balance_Pending.replace(/\,/g, "");
        formRecord.Balance_Pending = Balance_Pending;
        if (this.LiabilitiesLength <= this.MaxLimit) {
            this.LiabilitiesArray.push(Liabilities);
        }
        this.LiabilitiesLength = this.LiabilitiesArray.length;
        this.model.Liabilitiestype="0";
        this.model.Payable_Amount=null;
        this.model.Payment_Frequency=null;
        this.model.Balance_Pending=null;
        this.model.Financial_Institution=null;


    }
    onSubmitMain() {
         this.isLoading=!this.isLoading;
        this.model.app_id = this.application_id;
        this.model.assets = this.assetsDetails;
        this.model.Liabilities = this.LiabilitiesArray;
        this.model.asset_liability = true;
        this.model.skip = false;
        
        //chandan
        if(this.checkData){ 
            jQuery('#mlogin').show();//chandan
            this.successAccount();
        }else{
        //chandan

        // this.oaoService.OAOCreateOrUpdateHomeloanApplicant(this.model)
        //     .subscribe(
        //         data => {
        //         console.log(data);           
        //                 if(this.idCheck_v=="O"){
        //                     this.showSave();
        //                 }else if(this.idCheck_v=="M"){
        //                     this.router.navigate(['/OnlineIdCheck']);
        //                 }else{
        //                     this.successAccount();
        //                 }
        // 		}
        //      );
        switch (this.prod_code) {
            case 'HML': this.oaoService.OAOCreateOrUpdateHomeloanApplicant(this.model)
                .subscribe(
                data => {
                    console.log(data);
                    if (this.idCheck_v == "O") {
                        
                         
                              this.showSave(); //chandan
                       
                       
                    } else if (this.idCheck_v == "M") {
                        this.router.navigate(['/OnlineIdCheck']);
                    } else {
                        this.successAccount();
                    }
                }
                );
                break;
            case 'PRL': this.oaoService.OAOCreateOrUpdatePersonalloanApplicant(this.model)
                .subscribe(
                data => {
                    console.log(data);
                    if (this.idCheck_v == "O") {
                      
                              this.showSave(); //chandan
                        
                    } else if (this.idCheck_v == "M") {
                        this.router.navigate(['/OnlineIdCheck']);
                    } else {
                      
                        this.successAccount();
                    }
                }
                );
                break;
            default: console.log("Page not found");

        }

    }//else

    }
    showSave() {
        jQuery('#onlineid-check').modal('show');

    }
    public inf_code: string = '';
    successAccount() {
        this.model.skip = true;
        console.log(this.model);
        switch (this.prod_code) {
            case 'HML': this.oaoService.OAOCreateOrUpdateHomeloanApplicant(this.model)
                .subscribe(
                data => {
                    console.log(data);
                    localStorage.clear();
                    this.oaoService.GetPropertyDetails('INFO_MESSAGE', 'INF_004')
                        .subscribe(
                        data => {
                            this.inf_loan = data.result[0].property_value;
                            jQuery('#success').modal('show');
                        }
                        );

                }
                );
                break;
            case 'PRL': this.oaoService.OAOCreateOrUpdatePersonalloanApplicant(this.model)
                .subscribe(
                data => {
                    console.log(data);
                    localStorage.clear();
                    this.oaoService.GetPropertyDetails('INFO_MESSAGE', 'INF_005')
                        .subscribe(
                        data => {
                            this.inf_loan = data.result[0].property_value;
                            jQuery('#success').modal('show');
                        }
                        );

                }
                );
                break;
            default: console.log("Page not found");

        }


    }
    updateSection() {
        this.oaoService.updatesection("section_3", localStorage.getItem('application_id')).subscribe(
            data => {
                console.log(data);
                console.log("updated");
                this.router.navigate(['/IncomeExpense']);
            }
        );
    }

    deleteassets(index) {
        this.assetsDetails.splice(index, 1);
        this.assetsLength = this.assetsDetails.length;


    }
    deleteLiabilities(index) {
        this.LiabilitiesArray.splice(index, 1);
        this.LiabilitiesLength = this.LiabilitiesArray.length;

    }

    ngOnInit() {
         jQuery('#assettype').focus();
        jQuery('#mlogin').hide();//chandan

        this.assetsLength = this.assetsDetails.length;
        this.LiabilitiesLength = this.LiabilitiesArray.length;
        this.oaoService.GetPropertyDetails('commonCodes', 'ASSET_TYPE')
            .subscribe(
            data => {
                var count = Object.keys(data.result).length;
                for (var i = 0; i < count; i++) {
                    this.assetType.push({
                        prop_desc: data.result[i].property_desc,
                        prop_val: data.result[i].property_value
                    })
                }
            }
            );
        this.oaoService.GetPropertyDetails('commonCodes', 'LIABILITY_TYPE')
            .subscribe(
            data => {
                var count = Object.keys(data.result).length;
                for (var i = 0; i < count; i++) {
                    this.liabilityType.push({
                        prop_desc: data.result[i].property_desc,
                        prop_val: data.result[i].property_value
                    })
                }
            }
            );
        this.oaoService.GetPropertyDetails('commonCodes', 'FREQ_TYPE')
            .subscribe(
            data => {
                var count = Object.keys(data.result).length;
                for (var i = 0; i < count; i++) {
                    this.freqType.push({
                        prop_desc: data.result[i].property_desc,
                        prop_val: data.result[i].property_value
                    })
                }
            }
            );
    }
    clear() {
        window.location.reload();
        localStorage.clear();
    }
    liability_tab: boolean = false
    addClass() {
        jQuery('#assettype').blur();
        jQuery('#Liabilitiestype').focus();
        this.liability_tab = true
    }
    AmountFormatter(amountvalue: any, var_v: any) {
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'AUD',
            minimumFractionDigits: 2,
        });
        //     this.testmodel[var_v]="";
        //  this.testmodel[var_v]=amountvalue;
        var finalString = formatter.format(amountvalue);
		finalString = finalString.replace('A$','');
        this.model[var_v] = finalString.replace('$','');
    }

    revert(oldvalue: any, var_v: any) {
        var tmpOldvalue;
        if (oldvalue != null && String(oldvalue).match(/\,/g)) {
            tmpOldvalue = oldvalue.replace(/\,/g, '');
            console.log(tmpOldvalue);
            this.model[var_v] = tmpOldvalue.substr(0, tmpOldvalue.length - 3);
            console.log(this.model[var_v]);
        }
    }

}